import React from "react";
import {
  View,
  Text,
  ScrollView,
  Button,
  TouchableOpacity,
  TextInput,
  Picker,
  Animated,
  StyleSheet,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import Image from "react-native-remote-svg";
import { Col, Row, Grid } from "react-native-easy-grid";
import NestedListView, {NestedRow} from 'react-native-nested-listview';
import { 
  Bg,
  Container,
  BgText,
  Card,
  CardHeader,
  CardSubHeader,
  FormMainSearch,
  TextMainSearch,
  FormSecondarySearch,
  TextSecondarySearch,
  TextOrange,
  TextBlue,
  TextGray,
  BtnMainSearch,
  CardTitle,
  H1,
  H2,
  H3,
  H4,
  P,
  B,
  HR,
  FontSizeXXXL,
  FontSizeXXL,
  FontSizeXL,
  FontSizeL,
  FontSizeM,
  FontSizeS,
  FontSizeXS,
  FontSizeXXS,
  MarginXL,
  MarginL,
  MarginM,
  MarginS,
  MarginXS,
  MarginXXS,
  MarginTopXL,
  MarginTopL,
  MarginTopS,
  MarginTopXS,
  MarginTopXXS,
  MarginBottomXL,
  MarginBottomL,
  MarginBottomS,
  MarginBottomXS,
  MarginBottomXXS,
  MarginLeftXL,
  MarginLeftL,
  MarginLeftS,
  MarginLeftXS,
  MarginLeftXXS,
  MarginRightXL,
  MarginRightL,
  MarginRightS,
  MarginRightXS,
  MarginRightXXS,
  PaddingXL,
  PaddingL,
  PaddingM,
  PaddingS,
  PaddingXS,
  PaddingXXS,
  PaddingTopXL,
  PaddingTopL,
  PaddingTopS,
  PaddingTopXS,
  PaddingTopXXS,
  PaddingBottomXL,
  PaddingBottomL,
  PaddingBottomS,
  PaddingBottomXS,
  PaddingBottomXXS,
  PaddingLeftXL,
  PaddingLeftL,
  PaddingLeftS,
  PaddingLeftXS,
  PaddingLeftXXS,
  PaddingRightXL,
  PaddingRightL,
  PaddingRightS,
  PaddingRightXS,
  PaddingRightXXS,
  ButtonPrimaryM,
  ButtonPrimaryMDisabled,
  ButtonPrimaryMText,
  ButtonPrimaryInverseM,
  ButtonPrimaryInverseMText,
  ButtonGrayInverseM,
  ButtonGrayInverseMText,
  ButtonSecondaryM,
  ButtonSecondaryMDisabled,
  ButtonSecondaryMText,
  ButtonSecondaryInverseM,
  ButtonSecondaryInverseMText,
  Left,
  Center,
  Right,
  DistributeNyampingUjung,
  DistributeNyampingTengah,
  HeaderPills,
  FormM,
  FormMIcon,
  LabelForm,
  LeftM,
  InfoBoxPcp,
  NotifBadge,
  NotifText,
  ProductContainer,
  WidthFullSize,
  Priceold,
  Priceoldhr,
  PriceProduct,
  Price,
  TitleLimitName,
  DiscountContainer,
  TextDiscount,
  Wishlist,
  Promotion,
  IconCircle,
  ContainerS,
  TextCenter,
  PromotionText } from '../style/ruparupaUI'

const data = [{title: 'Dekorasi Dinding', items: [{title: 'Tampilkan Semua'},{title: 'Dekorasi Meja & Lantai'}, {title: 'Jam'}]},
              {title: 'Tempat Penyimpanan', items: [{title: 'Box'}, {title: 'Kotak Perhiasan'}]}            
]


class Kategori extends React.Component {

  static navigationOptions = {
    header: null,
    drawerIcon: ({tintColor}) => (
      <Ionicons
                name="ios-grid-outline"
                size={24}
                color="rgba(85, 87, 97, 1)"
                style={{color:tintColor}}
              />
    )
  }

  render() {
    return (
    <Bg>    
        <CardHeader>
            <DistributeNyampingUjung>
            <TouchableOpacity onPress={()=>this.props.navigation.openDrawer()}>
                <Ionicons
                  name="md-menu"
                  size={32}
                  color="rgba(85, 87, 97, 1)"
                  style={{ marginLeft: 5 }}
                />
              </TouchableOpacity>
              <FontSizeL> Kategori</FontSizeL>
              <TouchableOpacity>
                <Ionicons
                  name="md-search"
                  size={32}
                  color="rgba(85, 87, 97, 1)"
                  style={{ marginRight: 5 }}
                />
              </TouchableOpacity>
            </DistributeNyampingUjung>
          </CardHeader>
    <Grid>
        <Col style={{ width: 100 }}>
            <ScrollView
            showsVerticalScrollIndicator={false}
            >
                <MarginBottomXXS>
                    <TouchableOpacity>
                        <Card>
                            <ContainerS>
                                    <Ionicons
                                    name="ios-home-outline"
                                    size={24}
                                    color="rgba(85, 87, 97, 1)"
                                    style={{alignSelf:'center'}}
                                    />   
                            <TextCenter><P><B>Rumah Tangga</B></P></TextCenter>
                            </ContainerS>
                        </Card>
                    </TouchableOpacity>
                </MarginBottomXXS>
                <MarginBottomXXS>
                    <TouchableOpacity>
                        <ContainerS>
                                <Ionicons
                                name="ios-home-outline"
                                size={24}
                                color="rgba(85, 87, 97, 1)"
                                style={{alignSelf:'center'}}
                                />   
                        <TextCenter><P>Dapur Minimalis</P></TextCenter>
                        </ContainerS>
                    </TouchableOpacity>
                </MarginBottomXXS> 
                <MarginBottomXXS>
                    <TouchableOpacity>
                        <ContainerS>
                                <Ionicons
                                name="ios-home-outline"
                                size={24}
                                color="rgba(85, 87, 97, 1)"
                                style={{alignSelf:'center'}}
                                />   
                        <TextCenter><P>Furnitur</P></TextCenter>
                        </ContainerS>
                    </TouchableOpacity>
                </MarginBottomXXS> 
                <MarginBottomXXS>
                    <TouchableOpacity>
                        <ContainerS>
                                <Ionicons
                                name="ios-home-outline"
                                size={24}
                                color="rgba(85, 87, 97, 1)"
                                style={{alignSelf:'center'}}
                                />   
                        <TextCenter><P>Bed & Bath</P></TextCenter>
                        </ContainerS>
                    </TouchableOpacity>
                </MarginBottomXXS> 
                <MarginBottomXXS>
                    <TouchableOpacity>
                        <ContainerS>
                                <Ionicons
                                name="ios-home-outline"
                                size={24}
                                color="rgba(85, 87, 97, 1)"
                                style={{alignSelf:'center'}}
                                />   
                        <TextCenter><P>Home Improvement</P></TextCenter>
                        </ContainerS>
                    </TouchableOpacity>
                </MarginBottomXXS> 
                <MarginBottomXXS>
                    <TouchableOpacity>
                        <ContainerS>
                                <Ionicons
                                name="ios-home-outline"
                                size={24}
                                color="rgba(85, 87, 97, 1)"
                                style={{alignSelf:'center'}}
                                />   
                        <TextCenter><P>Otomotif</P></TextCenter>
                        </ContainerS>
                    </TouchableOpacity>
                </MarginBottomXXS> 
                <MarginBottomXXS>
                    <TouchableOpacity>
                        <ContainerS>
                                <Ionicons
                                name="ios-home-outline"
                                size={24}
                                color="rgba(85, 87, 97, 1)"
                                style={{alignSelf:'center'}}
                                />   
                        <TextCenter><P>Hobi & Gaya Hidup</P></TextCenter>
                        </ContainerS>
                    </TouchableOpacity>
                </MarginBottomXXS> 
                <MarginBottomXXS>
                    <TouchableOpacity>
                        <ContainerS>
                                <Ionicons
                                name="ios-home-outline"
                                size={24}
                                color="rgba(85, 87, 97, 1)"
                                style={{alignSelf:'center'}}
                                />   
                        <TextCenter><P>Kesehatan & Olahraga</P></TextCenter>
                        </ContainerS>
                    </TouchableOpacity>
                </MarginBottomXXS> 
                <MarginBottomXXS>
                    <TouchableOpacity>
                        <ContainerS>
                                <Ionicons
                                name="ios-home-outline"
                                size={24}
                                color="rgba(85, 87, 97, 1)"
                                style={{alignSelf:'center'}}
                                />   
                        <TextCenter><P>Elektronik & Gadget</P></TextCenter>
                        </ContainerS>
                    </TouchableOpacity>
                </MarginBottomXXS> 
                <MarginBottomXXS>
                    <TouchableOpacity>
                        <ContainerS>
                                <Ionicons
                                name="ios-home-outline"
                                size={24}
                                color="rgba(85, 87, 97, 1)"
                                style={{alignSelf:'center'}}
                                />   
                        <TextCenter><P>Mainan & Bayi</P></TextCenter>
                        </ContainerS>
                    </TouchableOpacity>
                </MarginBottomXXS> 
                <MarginBottomXXS>
                    <TouchableOpacity>
                        <ContainerS>
                                <Ionicons
                                name="ios-home-outline"
                                size={24}
                                color="rgba(85, 87, 97, 1)"
                                style={{alignSelf:'center'}}
                                />   
                        <TextCenter><P>New Arrivals</P></TextCenter>
                        </ContainerS>
                    </TouchableOpacity>
                </MarginBottomXXS> 
                <MarginBottomXXS>
                    <TouchableOpacity>
                        <ContainerS>
                                <Ionicons
                                name="ios-home-outline"
                                size={24}
                                color="rgba(85, 87, 97, 1)"
                                style={{alignSelf:'center'}}
                                />   
                        <TextCenter><P>Best Deal</P></TextCenter>
                        </ContainerS>
                    </TouchableOpacity>
                </MarginBottomXXS> 
            </ScrollView>
        </Col>
        <Col>
            <ScrollView>
                <Card>
                    <Container>    
                    <NestedListView
                        data={data}
                        getChildrenName={(node) => 'items'}
                        renderNode={(node, level) => (
                            <NestedRow
                            level={level}
                            >
                            <P>{node.title}</P>   
                            </NestedRow>
                        )}
                        />
                    </Container>
                </Card>
            </ScrollView>
        </Col>
    </Grid>
    </Bg>
    );
  }
}

export default Kategori;
