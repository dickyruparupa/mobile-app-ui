import React, {Component} from "react";
import {
  View,
  Text,
  ScrollView,
  Button,
  TouchableOpacity,
  TextInput,
  Picker,
  Animated,
  StyleSheet,
  Dimensions,
  Platform,
  Modal,
  TouchableHighlight,
  SafeAreaView
} from "react-native";
import { Ionicons , EvilIcons , FontAwesome } from "@expo/vector-icons";
import Image from "react-native-remote-svg";
import Swiper from 'react-native-swiper';
import { Col, Row, Grid } from "react-native-easy-grid";


import { 
  Bg,
  Container,
  BgText,
  Card,
  CardHeader,
  CardSubHeader,
  FormMainSearch,
  TextMainSearch,
  FormSecondarySearch,
  TextSecondarySearch,
  TextOrange,
  TextBlue,
  TextGray,
  BtnMainSearch,
  CardTitle,
  H1,
  H2,
  H3,
  H4,
  P,
  B,
  HR,
  FontSizeXXXL,
  FontSizeXXL,
  FontSizeXL,
  FontSizeL,
  FontSizeM,
  FontSizeS,
  FontSizeXS,
  FontSizeXXS,
  MarginXL,
  MarginL,
  MarginM,
  MarginS,
  MarginXS,
  MarginXXS,
  MarginTopXL,
  MarginTopL,
  MarginTopS,
  MarginTopXS,
  MarginTopXXS,
  MarginBottomXL,
  MarginBottomL,
  MarginBottomS,
  MarginBottomXS,
  MarginBottomXXS,
  MarginLeftXL,
  MarginLeftL,
  MarginLeftS,
  MarginLeftXS,
  MarginLeftXXS,
  MarginRightXL,
  MarginRightL,
  MarginRightS,
  MarginRightXS,
  MarginRightXXS,
  PaddingXL,
  PaddingL,
  PaddingM,
  PaddingS,
  PaddingXS,
  PaddingXXS,
  PaddingTopXL,
  PaddingTopL,
  PaddingTopS,
  PaddingTopXS,
  PaddingTopXXS,
  PaddingBottomXL,
  PaddingBottomL,
  PaddingBottomS,
  PaddingBottomXS,
  PaddingBottomXXS,
  PaddingLeftXL,
  PaddingLeftL,
  PaddingLeftS,
  PaddingLeftXS,
  PaddingLeftXXS,
  PaddingRightXL,
  PaddingRightL,
  PaddingRightS,
  PaddingRightXS,
  PaddingRightXXS,
  ButtonPrimaryM,
  ButtonPrimaryMDisabled,
  ButtonPrimaryMText,
  ButtonPrimaryInverseM,
  ButtonPrimaryInverseMText,
  ButtonGrayInverseM,
  ButtonGrayInverseMText,
  ButtonSecondaryM,
  ButtonSecondaryMDisabled,
  ButtonSecondaryMText,
  ButtonSecondaryInverseM,
  ButtonSecondaryInverseMText,
  Left,
  Center,
  Right,
  DistributeNyampingUjung,
  DistributeNyampingTengah,
  HeaderPills,
  FormM,
  FormMIcon,
  LabelForm,
  LeftM,
  InfoBoxPcp,
  NotifBadge,
  NotifText,
  ProductContainer,
  WidthFullSize,
  Priceold,
  Priceoldhr,
  PriceProduct,
  Price,
  TitleLimitName,
  DiscountContainer,
  TextDiscount,
  Wishlist,
  Promotion,
  IconCircle,
  PromotionText,
  StickyFooter,
  Bold,
  CardFlashSale,
  InspirationBGCard,
  FontWhite,
  TextCenter,
  BR,
  CardList,
  PromotionDelivery,
  CardDailyDeals } from '../style/ruparupaUI';
  import CountDown from 'react-native-countdown-component';
  import ExploreByCategory from '../components/explore/category';
  import ExploreByTrending from '../components/explore/trending';


  const { height,width } = Dimensions.get('window')


  var styles = {
    wrapper: {
      ...Platform.select({
        ios: {         
          height:height/3.6
        },
        android: {
          height:200
        },
      }),
  },
  slide: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
    image: {
      width,
      flex: 1
    }
  }

class HomePage extends React.Component {

  static navigationOptions = {
    header: null,
    drawerIcon: ({tintColor}) => (
      <Ionicons
                name="ios-home-outline"
                size={24}
                color="rgba(85, 87, 97, 1)"
                style={{color:tintColor}}
              />
    )  
  }
  state = {
    modalVisible: false,
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }
  


  render() {
    return (
      <Bg>
        <CardHeader>
          <DistributeNyampingUjung>
            <TouchableOpacity onPress={()=>this.props.navigation.openDrawer()}>
              <Ionicons
                name="md-menu"
                size={32}
                color="rgba(85, 87, 97, 1)"
                style={{}}
              />
            </TouchableOpacity>
            <Image
              source={{
                uri:
                  "https://res.cloudinary.com/ruparupa-com/image/upload/v1524564956/ruparupa-assets/img/ruparupa-logo.svg"
              }}
              style={{ height: 25, alignSelf: 'center' }}
            />
            <TouchableOpacity>
              <Ionicons
                name="md-cart"
                size={32}
                color="rgba(85, 87, 97, 1)"
                style={{}}
              />
            </TouchableOpacity>
          </DistributeNyampingUjung>
          <TouchableOpacity>
            <FormMainSearch>
              <DistributeNyampingUjung>
                <TextMainSearch>Cari ruparupa produk atau merek</TextMainSearch>
                <BtnMainSearch>
                  <Ionicons
                    name="md-search"
                    size={22}
                    color="rgba(255, 255, 255, 1)"
                    style={{ alignSelf: "center" }}
                  />
                </BtnMainSearch>
              </DistributeNyampingUjung>
            </FormMainSearch>
          </TouchableOpacity>
        </CardHeader>
        <ScrollView>
        <Swiper
            style={styles.wrapper}
            autoplay={true}
            loop={true}>
            <View style={styles.slide}>
                <Image style={styles.image} source={require('../img/banner1.jpg')} />
            </View>
            <View style={styles.slide}>
                <Image style={styles.image} source={require('../img/banner2.jpg')} />
            </View>
            <View style={styles.slide}>
                <Image style={styles.image} source={require('../img/banner3.jpg')} />
            </View>
            <View style={styles.slide}>
                <Image style={styles.image} source={require('../img/banner4.jpg')} />
            </View>
            <View style={styles.slide}>
                <Image style={styles.image} source={require('../img/banner5.jpg')} />
            </View>
          
        </Swiper>
        
        <MarginTopS>
        <Card>
          <Center>
          <FontSizeXL>
              <Bold>
                Penawaran Saat Ini
              </Bold>
          </FontSizeXL>
          </Center>
          <MarginTopXS>
          <Container>
          <Grid>
              <Col>
                  <Image
                  source={{
                    uri:
                      "https://res.cloudinary.com/ruparupa-com/image/upload/v1536307713/2.1/svg/ace-logo-mobile-app.svg"
                  }}
                  style={{ height: 60, alignSelf: 'center' }}
                  />
                  <MarginTopXS>
                    <TextCenter><P>Ace Hardware</P></TextCenter>
                  </MarginTopXS>  
              </Col>
              <Col>
                  <Image
                  source={{
                    uri:
                      "https://res.cloudinary.com/ruparupa-com/image/upload/v1535508327/promo/informa-wow-sale/icon.svg"
                  }}
                  style={{ height: 60, alignSelf: 'center' }}
                  />
                  <MarginTopXS>
                    <TextCenter><P>Informa WOW Sale</P></TextCenter>
                  </MarginTopXS>
              </Col>
              <Col>
                  <Image
                  source={{
                    uri:
                      "https://res.cloudinary.com/ruparupa-com/image/upload/v1526601233/2.1/svg/new-arrivals.svg"
                  }}
                  style={{ height: 60, alignSelf: 'center' }}
                  />
                  <MarginTopXS>
                    <TextCenter><P>New Arrivals</P></TextCenter>
                  </MarginTopXS>
              </Col>
          </Grid>
          </Container>
          </MarginTopXS>
          <MarginTopXS>
          <Container>
          <Grid>
              <Col>
                  <Image
                  source={{
                    uri:
                      "https://res.cloudinary.com/ruparupa-com/image/upload/v1523576694/2.1/flash-sale-icon.svg"
                  }}
                  style={{ height: 60, alignSelf: 'center' }}
                  />
                  <MarginTopXS>
                    <TextCenter><P>Setiap Kamis 10.00 WIB</P></TextCenter>
                  </MarginTopXS>  
              </Col>
              <Col>
                  <Image
                  source={{
                    uri:
                      "https://res.cloudinary.com/ruparupa-com/image/upload/v1523576706/2.1/promo-hari-ini.svg"
                  }}
                  style={{ height: 60, alignSelf: 'center' }}
                  />
                  <MarginTopXS>
                    <TextCenter><P>Semua Promosi</P></TextCenter>
                  </MarginTopXS>
              </Col>
              <Col>
                  <Image
                  source={{
                    uri:
                      "https://res.cloudinary.com/ruparupa-com/image/upload/v1523576694/2.1/best-deals.svg"
                  }}
                  style={{ height: 60, alignSelf: 'center' }}
                  />
                  <MarginTopXS>
                    <TextCenter><P>Best Deals</P></TextCenter>
                  </MarginTopXS>
              </Col>
          </Grid>
          </Container>
          </MarginTopXS>
        </Card> 
        </MarginTopS> 

        <MarginTopS>
        <CardFlashSale>
        <Center>
          <FontSizeXL>
              <Bold>
              <FontWhite>
                Flash Sale akan berakhir dalam
              </FontWhite>   
              </Bold>   
          </FontSizeXL>
          </Center>
          <MarginTopS>
          <CountDown
            until={500}
            onFinish={() => alert('Flash telah berakhir')}
            size={28}
            timeToShow={['H','M','S']}
            digitBgColor='#555761'
            digitTxtColor='white'
            timeTxtColor='white'
          />
          </MarginTopS>
          
          <TouchableHighlight
          onPress={() => {
            this.setModalVisible(true);
          }}>
          <Center>
          <FontSizeS>
          <FontWhite>Syarat & Ketentuan &nbsp;
            <Ionicons
                name="ios-help-circle"
                size={16}
                color="rgba(255, 255, 255, 1)"
                style={{}}
              />
            </FontWhite>
          </FontSizeS>
          </Center>
          </TouchableHighlight> 
       
          <MarginTopXS>
          <WidthFullSize>
              <ProductContainer>
                <Right>
                  <Wishlist>
                    <EvilIcons name="heart" size={24} color="rgba(85, 87, 97, 1)" style={{ alignSelf: "flex-start" }} />
                  </Wishlist>
                </Right>
                <Image
                  source={{
                    uri:
                      "https://res.cloudinary.com/ruparupa-com/image/upload/w_170,h_170,f_auto/v1510165192/Products/10119801_1.jpg"
                  }}
                  style={{ flex:1, height: 150 }}
                  resizeMode="contain"
                />
                <PriceProduct>
                  <Priceold>
                    <FontSizeXS>Rp 99.999.999</FontSizeXS>
                  </Priceold>
                  <Price>Rp 12.999.999</Price>
                  <DiscountContainer>
                    <TextDiscount>99%</TextDiscount>
                  </DiscountContainer>
                </PriceProduct>
                <TitleLimitName>
                  Di jual kursi-kursian dengan meja-mejaan bodo amat
                </TitleLimitName>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="ios-information-circle-outline"
                      size={14}
                      color="#F3251D"
                      />
                    </IconCircle>
                    <PromotionText>
                      Lowest Price
                    </PromotionText>
                  </Promotion>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="logo-dropbox"
                      size={16}
                      color="#757886"
                      />
                    </IconCircle>
                    <PromotionDelivery>
                      Someday Delivery
                    </PromotionDelivery>
                  </Promotion>
              </ProductContainer>
              <ProductContainer>
                <Right>
                  <Wishlist>
                    <EvilIcons name="heart" size={24} color="rgba(85, 87, 97, 1)" style={{ alignSelf: "flex-start" }} />
                  </Wishlist>
                </Right>
                <Image
                  source={{
                    uri:
                      "https://res.cloudinary.com/ruparupa-com/image/upload/w_170,h_170,f_auto/v1510165192/Products/10119801_1.jpg"
                  }}
                  style={{ flex:1, height: 150 }}
                  resizeMode="contain"
                />
                <PriceProduct>
                  <Priceold>
                    <FontSizeXS>Rp 99.999.999</FontSizeXS>
                  </Priceold>
                  <Price>Rp 12.999.999</Price>
                  <DiscountContainer>
                    <TextDiscount>99%</TextDiscount>
                  </DiscountContainer>
                </PriceProduct>
                <TitleLimitName>
                  Di jual kursi-kursian dengan meja-mejaan bodo amat
                </TitleLimitName>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="ios-information-circle-outline"
                      size={14}
                      color="#F3251D"
                      />
                    </IconCircle>
                    <PromotionText>
                      Lowest Price
                    </PromotionText>
                  </Promotion>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="logo-dropbox"
                      size={16}
                      color="#757886"
                      />
                    </IconCircle>
                    <PromotionDelivery>
                      Someday Delivery
                    </PromotionDelivery>
                  </Promotion>
              </ProductContainer>
          </WidthFullSize>
          <WidthFullSize>
              <ProductContainer>
                <Right>
                  <Wishlist>
                    <EvilIcons name="heart" size={24} color="rgba(85, 87, 97, 1)" style={{ alignSelf: "flex-start" }} />
                  </Wishlist>
                </Right>
                <Image
                  source={{
                    uri:
                      "https://res.cloudinary.com/ruparupa-com/image/upload/w_170,h_170,f_auto/v1510165192/Products/10119801_1.jpg"
                  }}
                  style={{ flex:1, height: 150 }}
                  resizeMode="contain"
                />
                <PriceProduct>
                  <Priceold>
                    <FontSizeXS>Rp 99.999.999</FontSizeXS>
                  </Priceold>
                  <Price>Rp 12.999.999</Price>
                  <DiscountContainer>
                    <TextDiscount>99%</TextDiscount>
                  </DiscountContainer>
                </PriceProduct>
                <TitleLimitName>
                  Di jual kursi-kursian dengan meja-mejaan bodo amat
                </TitleLimitName>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="ios-information-circle-outline"
                      size={14}
                      color="#F3251D"
                      />
                    </IconCircle>
                    <PromotionText>
                      Lowest Price
                    </PromotionText>
                  </Promotion>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="logo-dropbox"
                      size={16}
                      color="#757886"
                      />
                    </IconCircle>
                    <PromotionDelivery>
                      Someday Delivery
                    </PromotionDelivery>
                  </Promotion>
              </ProductContainer>
              <ProductContainer>
                <Right>
                  <Wishlist>
                    <EvilIcons name="heart" size={24} color="rgba(85, 87, 97, 1)" style={{ alignSelf: "flex-start" }} />
                  </Wishlist>
                </Right>
                <Image
                  source={{
                    uri:
                      "https://res.cloudinary.com/ruparupa-com/image/upload/w_170,h_170,f_auto/v1510165192/Products/10119801_1.jpg"
                  }}
                  style={{ flex:1, height: 150 }}
                  resizeMode="contain"
                />
                <PriceProduct>
                  <Priceold>
                    <FontSizeXS>Rp 99.999.999</FontSizeXS>
                  </Priceold>
                  <Price>Rp 12.999.999</Price>
                  <DiscountContainer>
                    <TextDiscount>99%</TextDiscount>
                  </DiscountContainer>
                </PriceProduct>
                <TitleLimitName>
                  Di jual kursi-kursian dengan meja-mejaan bodo amat
                </TitleLimitName>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="ios-information-circle-outline"
                      size={14}
                      color="#F3251D"
                      />
                    </IconCircle>
                    <PromotionText>
                      Lowest Price
                    </PromotionText>
                  </Promotion>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="logo-dropbox"
                      size={16}
                      color="#757886"
                      />
                    </IconCircle>
                    <PromotionDelivery>
                      Someday Delivery
                    </PromotionDelivery>
                  </Promotion>
              </ProductContainer>
          </WidthFullSize> 
          <WidthFullSize>
              <ProductContainer>
                <Right>
                  <Wishlist>
                    <EvilIcons name="heart" size={24} color="rgba(85, 87, 97, 1)" style={{ alignSelf: "flex-start" }} />
                  </Wishlist>
                </Right>
                <Image
                  source={{
                    uri:
                      "https://res.cloudinary.com/ruparupa-com/image/upload/w_170,h_170,f_auto/v1510165192/Products/10119801_1.jpg"
                  }}
                  style={{ flex:1, height: 150 }}
                  resizeMode="contain"
                />
                <PriceProduct>
                  <Priceold>
                    <FontSizeXS>Rp 99.999.999</FontSizeXS>
                  </Priceold>
                  <Price>Rp 12.999.999</Price>
                  <DiscountContainer>
                    <TextDiscount>99%</TextDiscount>
                  </DiscountContainer>
                </PriceProduct>
                <TitleLimitName>
                  Di jual kursi-kursian dengan meja-mejaan bodo amat
                </TitleLimitName>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="ios-information-circle-outline"
                      size={14}
                      color="#F3251D"
                      />
                    </IconCircle>
                    <PromotionText>
                      Lowest Price
                    </PromotionText>
                  </Promotion>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="logo-dropbox"
                      size={16}
                      color="#757886"
                      />
                    </IconCircle>
                    <PromotionDelivery>
                      Someday Delivery
                    </PromotionDelivery>
                  </Promotion>
              </ProductContainer>
              <ProductContainer>
                <Right>
                  <Wishlist>
                    <EvilIcons name="heart" size={24} color="rgba(85, 87, 97, 1)" style={{ alignSelf: "flex-start" }} />
                  </Wishlist>
                </Right>
                <Image
                  source={{
                    uri:
                      "https://res.cloudinary.com/ruparupa-com/image/upload/w_170,h_170,f_auto/v1510165192/Products/10119801_1.jpg"
                  }}
                  style={{ flex:1, height: 150 }}
                  resizeMode="contain"
                />
                <PriceProduct>
                  <Priceold>
                    <FontSizeXS>Rp 99.999.999</FontSizeXS>
                  </Priceold>
                  <Price>Rp 12.999.999</Price>
                  <DiscountContainer>
                    <TextDiscount>99%</TextDiscount>
                  </DiscountContainer>
                </PriceProduct>
                <TitleLimitName>
                  Di jual kursi-kursian dengan meja-mejaan bodo amat
                </TitleLimitName>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="ios-information-circle-outline"
                      size={14}
                      color="#F3251D"
                      />
                    </IconCircle>
                    <PromotionText>
                      Lowest Price
                    </PromotionText>
                  </Promotion>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="logo-dropbox"
                      size={16}
                      color="#757886"
                      />
                    </IconCircle>
                    <PromotionDelivery>
                      Someday Delivery
                    </PromotionDelivery>
                  </Promotion>
              </ProductContainer>
          </WidthFullSize>
          </MarginTopXS>
        </CardFlashSale> 
        </MarginTopS>

        <MarginTopS>
        <CardDailyDeals>
        <Center>
          <FontSizeXL>
              <Bold>
              <FontWhite>
                Daily Deals
              </FontWhite>   
              </Bold>   
          </FontSizeXL>
          </Center>
          <MarginTopS>
          <CountDown
            until={500}
            onFinish={() => alert('Flash telah berakhir')}
            size={18}
            timeToShow={['H','M','S']}
            digitBgColor='#555761'
            digitTxtColor='white'
            timeTxtColor='white'
          />
          </MarginTopS>
          <View style={{marginTop:5}}>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
            >
              
            <View style={{ width:150, marginLeft:11,  borderRadius:3, backgroundColor:'#FFFFFF'}}>
                <View style={{flex:3}}>
                <Image source={{uri: 'https://res.cloudinary.com/ruparupa-com/image/upload/w_360,h_360,f_auto/v1507054880/Products/10120528_1.jpg'}}
                style={{flex:1, width:null, height:null, resizeMode:'cover', height:150}}
                />
                </View>
                <View style={{flex:3, padding:5, marginTop:5}}>
                <PriceProduct>
                  <Priceold>
                    <FontSizeXS>Rp 99.999.999</FontSizeXS>
                  </Priceold>
                  <Price>Rp 12.999.999</Price>
                  <DiscountContainer>
                    <TextDiscount>99%</TextDiscount>
                  </DiscountContainer>
                </PriceProduct>
                <TitleLimitName>
                  Di jual kursi-kursian dengan meja-mejaan bodo amat
                </TitleLimitName>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="ios-information-circle-outline"
                      size={14}
                      color="#F3251D"
                      />
                    </IconCircle>
                    <PromotionText>
                      Lowest Price
                    </PromotionText>
                  </Promotion>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="logo-dropbox"
                      size={16}
                      color="#757886"
                      />
                    </IconCircle>
                    <PromotionDelivery>
                      Someday Delivery
                    </PromotionDelivery>
                  </Promotion>
                </View>  
            </View>  

            <View style={{ width:150, marginLeft:11,  borderRadius:3, backgroundColor:'#FFFFFF'}}>
                <View style={{flex:3}}>
                <Image source={{uri: 'https://res.cloudinary.com/ruparupa-com/image/upload/w_360,h_360,f_auto/v1507054880/Products/10120528_1.jpg'}}
                style={{flex:1, width:null, height:null, resizeMode:'cover', height:150}}
                />
                </View>
                <View style={{flex:3, padding:5, marginTop:5}}>
                <PriceProduct>
                  <Priceold>
                    <FontSizeXS>Rp 99.999.999</FontSizeXS>
                  </Priceold>
                  <Price>Rp 12.999.999</Price>
                  <DiscountContainer>
                    <TextDiscount>99%</TextDiscount>
                  </DiscountContainer>
                </PriceProduct>
                <TitleLimitName>
                  Di jual kursi-kursian dengan meja-mejaan bodo amat
                </TitleLimitName>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="ios-information-circle-outline"
                      size={14}
                      color="#F3251D"
                      />
                    </IconCircle>
                    <PromotionText>
                      Lowest Price
                    </PromotionText>
                  </Promotion>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="logo-dropbox"
                      size={16}
                      color="#757886"
                      />
                    </IconCircle>
                    <PromotionDelivery>
                      Someday Delivery
                    </PromotionDelivery>
                  </Promotion>
                </View>  
            </View>  

            <View style={{ width:150, marginLeft:11,  borderRadius:3, backgroundColor:'#FFFFFF'}}>
                <View style={{flex:3}}>
                <Image source={{uri: 'https://res.cloudinary.com/ruparupa-com/image/upload/w_360,h_360,f_auto/v1507054880/Products/10120528_1.jpg'}}
                style={{flex:1, width:null, height:null, resizeMode:'cover', height:150}}
                />
                </View>
                <View style={{flex:3, padding:5, marginTop:5}}>
                <PriceProduct>
                  <Priceold>
                    <FontSizeXS>Rp 99.999.999</FontSizeXS>
                  </Priceold>
                  <Price>Rp 12.999.999</Price>
                  <DiscountContainer>
                    <TextDiscount>99%</TextDiscount>
                  </DiscountContainer>
                </PriceProduct>
                <TitleLimitName>
                  Di jual kursi-kursian dengan meja-mejaan bodo amat
                </TitleLimitName>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="ios-information-circle-outline"
                      size={14}
                      color="#F3251D"
                      />
                    </IconCircle>
                    <PromotionText>
                      Lowest Price
                    </PromotionText>
                  </Promotion>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="logo-dropbox"
                      size={16}
                      color="#757886"
                      />
                    </IconCircle>
                    <PromotionDelivery>
                      Someday Delivery
                    </PromotionDelivery>
                  </Promotion>
                </View>  
            </View>  

            <View style={{ width:150, marginLeft:11,  borderRadius:3, backgroundColor:'#FFFFFF'}}>
                <View style={{flex:3}}>
                <Image source={{uri: 'https://res.cloudinary.com/ruparupa-com/image/upload/w_360,h_360,f_auto/v1507054880/Products/10120528_1.jpg'}}
                style={{flex:1, width:null, height:null, resizeMode:'cover', height:150}}
                />
                </View>
                <View style={{flex:3, padding:5, marginTop:5}}>
                <PriceProduct>
                  <Priceold>
                    <FontSizeXS>Rp 99.999.999</FontSizeXS>
                  </Priceold>
                  <Price>Rp 12.999.999</Price>
                  <DiscountContainer>
                    <TextDiscount>99%</TextDiscount>
                  </DiscountContainer>
                </PriceProduct>
                <TitleLimitName>
                  Di jual kursi-kursian dengan meja-mejaan bodo amat
                </TitleLimitName>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="ios-information-circle-outline"
                      size={14}
                      color="#F3251D"
                      />
                    </IconCircle>
                    <PromotionText>
                      Lowest Price
                    </PromotionText>
                  </Promotion>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="logo-dropbox"
                      size={16}
                      color="#757886"
                      />
                    </IconCircle>
                    <PromotionDelivery>
                      Someday Delivery
                    </PromotionDelivery>
                  </Promotion>
                </View>  
            </View>  

            <View style={{ width:150, marginLeft:11,  borderRadius:3, backgroundColor:'#FFFFFF'}}>
                <View style={{flex:3}}>
                <Image source={{uri: 'https://res.cloudinary.com/ruparupa-com/image/upload/w_360,h_360,f_auto/v1507054880/Products/10120528_1.jpg'}}
                style={{flex:1, width:null, height:null, resizeMode:'cover', height:150}}
                />
                </View>
                <View style={{flex:3, padding:5, marginTop:5}}>
                <PriceProduct>
                  <Priceold>
                    <FontSizeXS>Rp 99.999.999</FontSizeXS>
                  </Priceold>
                  <Price>Rp 12.999.999</Price>
                  <DiscountContainer>
                    <TextDiscount>99%</TextDiscount>
                  </DiscountContainer>
                </PriceProduct>
                <TitleLimitName>
                  Di jual kursi-kursian dengan meja-mejaan bodo amat
                </TitleLimitName>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="ios-information-circle-outline"
                      size={14}
                      color="#F3251D"
                      />
                    </IconCircle>
                    <PromotionText>
                      Lowest Price
                    </PromotionText>
                  </Promotion>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="logo-dropbox"
                      size={16}
                      color="#757886"
                      />
                    </IconCircle>
                    <PromotionDelivery>
                      Someday Delivery
                    </PromotionDelivery>
                  </Promotion>
                </View>  
            </View>  
            </ScrollView>
          </View> 
        </CardDailyDeals> 
        </MarginTopS>

        <MarginTopS>
        <Image
          style={{...Platform.select({
            ios: {         
              height:height/3.6
            },
            android: {
              height:200
            },
          }),}}
          source={{uri: 'https://res.cloudinary.com/ruparupa-com/image/upload/w_640,h_320,f_auto,fl_lossy/v1535515618/category-promo-brand/informa-wow-sale-thumbnail-mobile.jpg'}}
        />
        <Card>
        <View style={{marginTop:5}}>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
            >
              
            <View style={{ width:150, marginLeft:11,  borderRadius:3, backgroundColor:'#FFFFFF'}}>
                <View style={{flex:3}}>
                <Image source={{uri: 'https://res.cloudinary.com/ruparupa-com/image/upload/w_360,h_360,f_auto/v1507054880/Products/10120528_1.jpg'}}
                style={{flex:1, width:null, height:null, resizeMode:'cover', height:150}}
                />
                </View>
                <View style={{flex:3, padding:5, marginTop:5}}>
                <PriceProduct>
                  <Priceold>
                    <FontSizeXS>Rp 99.999.999</FontSizeXS>
                  </Priceold>
                  <Price>Rp 12.999.999</Price>
                  <DiscountContainer>
                    <TextDiscount>99%</TextDiscount>
                  </DiscountContainer>
                </PriceProduct>
                <TitleLimitName>
                  Di jual kursi-kursian dengan meja-mejaan bodo amat
                </TitleLimitName>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="ios-information-circle-outline"
                      size={14}
                      color="#F3251D"
                      />
                    </IconCircle>
                    <PromotionText>
                      Lowest Price
                    </PromotionText>
                  </Promotion>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="logo-dropbox"
                      size={16}
                      color="#757886"
                      />
                    </IconCircle>
                    <PromotionDelivery>
                      Someday Delivery
                    </PromotionDelivery>
                  </Promotion>
                </View>  
            </View>  

            <View style={{ width:150, marginLeft:11,  borderRadius:3, backgroundColor:'#FFFFFF'}}>
                <View style={{flex:3}}>
                <Image source={{uri: 'https://res.cloudinary.com/ruparupa-com/image/upload/w_360,h_360,f_auto/v1507054880/Products/10120528_1.jpg'}}
                style={{flex:1, width:null, height:null, resizeMode:'cover', height:150}}
                />
                </View>
                <View style={{flex:3, padding:5, marginTop:5}}>
                <PriceProduct>
                  <Priceold>
                    <FontSizeXS>Rp 99.999.999</FontSizeXS>
                  </Priceold>
                  <Price>Rp 12.999.999</Price>
                  <DiscountContainer>
                    <TextDiscount>99%</TextDiscount>
                  </DiscountContainer>
                </PriceProduct>
                <TitleLimitName>
                  Di jual kursi-kursian dengan meja-mejaan bodo amat
                </TitleLimitName>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="ios-information-circle-outline"
                      size={14}
                      color="#F3251D"
                      />
                    </IconCircle>
                    <PromotionText>
                      Lowest Price
                    </PromotionText>
                  </Promotion>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="logo-dropbox"
                      size={16}
                      color="#757886"
                      />
                    </IconCircle>
                    <PromotionDelivery>
                      Someday Delivery
                    </PromotionDelivery>
                  </Promotion>
                </View>  
            </View>  

            <View style={{ width:150, marginLeft:11,  borderRadius:3, backgroundColor:'#FFFFFF'}}>
                <View style={{flex:3}}>
                <Image source={{uri: 'https://res.cloudinary.com/ruparupa-com/image/upload/w_360,h_360,f_auto/v1507054880/Products/10120528_1.jpg'}}
                style={{flex:1, width:null, height:null, resizeMode:'cover', height:150}}
                />
                </View>
                <View style={{flex:3, padding:5, marginTop:5}}>
                <PriceProduct>
                  <Priceold>
                    <FontSizeXS>Rp 99.999.999</FontSizeXS>
                  </Priceold>
                  <Price>Rp 12.999.999</Price>
                  <DiscountContainer>
                    <TextDiscount>99%</TextDiscount>
                  </DiscountContainer>
                </PriceProduct>
                <TitleLimitName>
                  Di jual kursi-kursian dengan meja-mejaan bodo amat
                </TitleLimitName>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="ios-information-circle-outline"
                      size={14}
                      color="#F3251D"
                      />
                    </IconCircle>
                    <PromotionText>
                      Lowest Price
                    </PromotionText>
                  </Promotion>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="logo-dropbox"
                      size={16}
                      color="#757886"
                      />
                    </IconCircle>
                    <PromotionDelivery>
                      Someday Delivery
                    </PromotionDelivery>
                  </Promotion>
                </View>  
            </View>  

            <View style={{ width:150, marginLeft:11,  borderRadius:3, backgroundColor:'#FFFFFF'}}>
                <View style={{flex:3}}>
                <Image source={{uri: 'https://res.cloudinary.com/ruparupa-com/image/upload/w_360,h_360,f_auto/v1507054880/Products/10120528_1.jpg'}}
                style={{flex:1, width:null, height:null, resizeMode:'cover', height:150}}
                />
                </View>
                <View style={{flex:3, padding:5, marginTop:5}}>
                <PriceProduct>
                  <Priceold>
                    <FontSizeXS>Rp 99.999.999</FontSizeXS>
                  </Priceold>
                  <Price>Rp 12.999.999</Price>
                  <DiscountContainer>
                    <TextDiscount>99%</TextDiscount>
                  </DiscountContainer>
                </PriceProduct>
                <TitleLimitName>
                  Di jual kursi-kursian dengan meja-mejaan bodo amat
                </TitleLimitName>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="ios-information-circle-outline"
                      size={14}
                      color="#F3251D"
                      />
                    </IconCircle>
                    <PromotionText>
                      Lowest Price
                    </PromotionText>
                  </Promotion>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="logo-dropbox"
                      size={16}
                      color="#757886"
                      />
                    </IconCircle>
                    <PromotionDelivery>
                      Someday Delivery
                    </PromotionDelivery>
                  </Promotion>
                </View>  
            </View>  

            <View style={{ width:150, marginLeft:11,  borderRadius:3, backgroundColor:'#FFFFFF'}}>
                <View style={{flex:3}}>
                <Image source={{uri: 'https://res.cloudinary.com/ruparupa-com/image/upload/w_360,h_360,f_auto/v1507054880/Products/10120528_1.jpg'}}
                style={{flex:1, width:null, height:null, resizeMode:'cover', height:150}}
                />
                </View>
                <View style={{flex:3, padding:5, marginTop:5}}>
                <PriceProduct>
                  <Priceold>
                    <FontSizeXS>Rp 99.999.999</FontSizeXS>
                  </Priceold>
                  <Price>Rp 12.999.999</Price>
                  <DiscountContainer>
                    <TextDiscount>99%</TextDiscount>
                  </DiscountContainer>
                </PriceProduct>
                <TitleLimitName>
                  Di jual kursi-kursian dengan meja-mejaan bodo amat
                </TitleLimitName>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="ios-information-circle-outline"
                      size={14}
                      color="#F3251D"
                      />
                    </IconCircle>
                    <PromotionText>
                      Lowest Price
                    </PromotionText>
                  </Promotion>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="logo-dropbox"
                      size={16}
                      color="#757886"
                      />
                    </IconCircle>
                    <PromotionDelivery>
                      Someday Delivery
                    </PromotionDelivery>
                  </Promotion>
                </View>  
            </View>  

            </ScrollView>
          </View> 
          <Container>
          <ButtonSecondaryInverseM>
              <ButtonSecondaryInverseMText>
                Lihat Semua (380+ Items)
              </ButtonSecondaryInverseMText>
            </ButtonSecondaryInverseM>
          </Container>  
        </Card>
        </MarginTopS>

        <MarginTopXXS>
          <Container>
          <FontSizeXL>
            <Bold>Belanja per Kategori</Bold>
          </FontSizeXL>
          </Container>
          <View style={{height:135, marginTop:5}}>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
            >
              <ExploreByCategory imageUri={require('../img/cat1.jpg')}
              name="Kursi Unik untuk Hunian"
              />
              <ExploreByCategory imageUri={require('../img/cat2.jpg')}
              name="Tampilan Sofa Lebih Menawan"
              />  
              <ExploreByCategory imageUri={require('../img/cat3.jpg')}
              name="Rak & Kabinet"
              />  
              <ExploreByCategory imageUri={require('../img/cat4.jpg')}
              name="Smart Lighting & Living by ACE"
              />
              <ExploreByCategory imageUri={require('../img/cat5.jpg')}
              name="Buy One Get One"
              />  
              <ExploreByCategory imageUri={require('../img/cat6.jpg')}
              name="Penawaran Spesial Online"
              />  
              <ExploreByCategory imageUri={require('../img/cat7.jpg')}
              name="Harga Paling Murah"
              />      
              <ExploreByCategory imageUri={require('../img/cat8.jpg')}
              name="Produk Terlaris"
              />  
            </ScrollView>
          </View>  
        </MarginTopXXS>  

        <MarginTopXXS>
          <Container>
          <FontSizeXL>
            <Bold>People's Choice</Bold>
          </FontSizeXL>  
          </Container>
          <View style={{height:180, marginTop:5}}>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
            >
              <ExploreByTrending imageUri={require('../img/trend1.jpg')}
              name="Kursi"
              />
              <ExploreByTrending imageUri={require('../img/trend2.jpg')}
              name="Kursi Nuansa Skandanavia"
              />  
              <ExploreByTrending imageUri={require('../img/trend3.jpg')}
              name="Sofa Bed"
              />  
              <ExploreByTrending imageUri={require('../img/trend4.jpg')}
              name="Voucher"
              />
              <ExploreByTrending imageUri={require('../img/trend5.jpg')}
              name="Kipas Angin"
              />  
              <ExploreByTrending imageUri={require('../img/trend6.jpg')}
              name="Kendaraan Bersih Sebelum Hujan"
              />  
              <ExploreByTrending imageUri={require('../img/trend7.jpg')}
              name="Sikat & Spons"
              />      
              <ExploreByTrending imageUri={require('../img/trend8.jpg')}
              name="Pipa & Keran"
              />  
              <ExploreByTrending imageUri={require('../img/trend9.jpg')}
              name="Tangga Hemat 50%"
              />  
              <ExploreByTrending imageUri={require('../img/trend10.jpg')}
              name="Mainan Outdoor"
              />  
              <ExploreByTrending imageUri={require('../img/trend11.jpg')}
              name="Stoples Unik"
              />
              <ExploreByTrending imageUri={require('../img/trend12.jpg')}
              name="Toys Kingdom"
              />    
            </ScrollView>
          </View>  
        </MarginTopXXS>

        <MarginTopXXS>
          <Container>
          <Center>  
          <FontSizeXL>
            <Bold>Inspirasi Belanja</Bold>
          </FontSizeXL> 
          </Center> 
          </Container>
        </MarginTopXXS>
          
        {/* <MarginTopXXS>
        <Image
          style={{...Platform.select({
            ios: {         
              height:height/3.6
            },
            android: {
              height:200
            },
            backgroundColor:'red'
          }),}}
          source={{uri: 'https://res.cloudinary.com/ruparupa-com/image/upload/f_auto,fl_lossy,w_640/v1535357785/category-inspirations/patterned-classy-thumbnail.jpg'}}
        />
        <InspirationBGCard>
        <FontSizeL><Bold><FontWhite>Patterned & Classy</FontWhite></Bold></FontSizeL>
        </InspirationBGCard>
        <Card>
        <WidthFullSize>
              <ProductContainer>
                <Right>
                  <Wishlist>
                    <EvilIcons name="heart" size={24} color="rgba(85, 87, 97, 1)" style={{ alignSelf: "flex-start" }} />
                  </Wishlist>
                </Right>
                <Image
                  source={{
                    uri:
                      "https://res.cloudinary.com/ruparupa-com/image/upload/w_170,h_170,f_auto/v1510165192/Products/10119801_1.jpg"
                  }}
                  style={{ flex:1, height: 150 }}
                  resizeMode="contain"
                />
                <PriceProduct>
                  <Priceold>
                    <FontSizeXS>Rp 99.999.999</FontSizeXS>
                  </Priceold>
                  <Price>Rp 12.999.999</Price>
                  <DiscountContainer>
                    <TextDiscount>99%</TextDiscount>
                  </DiscountContainer>
                </PriceProduct>
                <TitleLimitName>
                  Di jual kursi-kursian dengan meja-mejaan bodo amat
                </TitleLimitName>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="ios-information-circle-outline"
                      size={14}
                      color="#F3251D"
                      />
                    </IconCircle>
                    <PromotionText>
                      Lowest Price
                    </PromotionText>
                  </Promotion>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="logo-dropbox"
                      size={16}
                      color="#757886"
                      />
                    </IconCircle>
                    <PromotionDelivery>
                      Someday Delivery
                    </PromotionDelivery>
                  </Promotion>
              </ProductContainer>
              <ProductContainer>
                <Right>
                  <Wishlist>
                    <EvilIcons name="heart" size={24} color="rgba(85, 87, 97, 1)" style={{ alignSelf: "flex-start" }} />
                  </Wishlist>
                </Right>
                <Image
                  source={{
                    uri:
                      "https://res.cloudinary.com/ruparupa-com/image/upload/w_170,h_170,f_auto/v1510165192/Products/10119801_1.jpg"
                  }}
                  style={{ flex:1, height: 150 }}
                  resizeMode="contain"
                />
                <PriceProduct>
                  <Priceold>
                    <FontSizeXS>Rp 99.999.999</FontSizeXS>
                  </Priceold>
                  <Price>Rp 12.999.999</Price>
                  <DiscountContainer>
                    <TextDiscount>99%</TextDiscount>
                  </DiscountContainer>
                </PriceProduct>
                <TitleLimitName>
                  Di jual kursi-kursian dengan meja-mejaan bodo amat
                </TitleLimitName>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="ios-information-circle-outline"
                      size={14}
                      color="#F3251D"
                      />
                    </IconCircle>
                    <PromotionText>
                      Lowest Price
                    </PromotionText>
                  </Promotion>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="logo-dropbox"
                      size={16}
                      color="#757886"
                      />
                    </IconCircle>
                    <PromotionDelivery>
                      Someday Delivery
                    </PromotionDelivery>
                  </Promotion>
              </ProductContainer>
          </WidthFullSize>
          <WidthFullSize>
              <ProductContainer>
                <Right>
                  <Wishlist>
                    <EvilIcons name="heart" size={24} color="rgba(85, 87, 97, 1)" style={{ alignSelf: "flex-start" }} />
                  </Wishlist>
                </Right>
                <Image
                  source={{
                    uri:
                      "https://res.cloudinary.com/ruparupa-com/image/upload/w_170,h_170,f_auto/v1510165192/Products/10119801_1.jpg"
                  }}
                  style={{ flex:1, height: 150 }}
                  resizeMode="contain"
                />
                <PriceProduct>
                  <Priceold>
                    <FontSizeXS>Rp 99.999.999</FontSizeXS>
                  </Priceold>
                  <Price>Rp 12.999.999</Price>
                  <DiscountContainer>
                    <TextDiscount>99%</TextDiscount>
                  </DiscountContainer>
                </PriceProduct>
                <TitleLimitName>
                  Di jual kursi-kursian dengan meja-mejaan bodo amat
                </TitleLimitName>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="ios-information-circle-outline"
                      size={14}
                      color="#F3251D"
                      />
                    </IconCircle>
                    <PromotionText>
                      Lowest Price
                    </PromotionText>
                  </Promotion>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="logo-dropbox"
                      size={16}
                      color="#757886"
                      />
                    </IconCircle>
                    <PromotionDelivery>
                      Someday Delivery
                    </PromotionDelivery>
                  </Promotion>
              </ProductContainer>
              <ProductContainer>
                <Right>
                  <Wishlist>
                    <EvilIcons name="heart" size={24} color="rgba(85, 87, 97, 1)" style={{ alignSelf: "flex-start" }} />
                  </Wishlist>
                </Right>
                <Image
                  source={{
                    uri:
                      "https://res.cloudinary.com/ruparupa-com/image/upload/w_170,h_170,f_auto/v1510165192/Products/10119801_1.jpg"
                  }}
                  style={{ flex:1, height: 150 }}
                  resizeMode="contain"
                />
                <PriceProduct>
                  <Priceold>
                    <FontSizeXS>Rp 99.999.999</FontSizeXS>
                  </Priceold>
                  <Price>Rp 12.999.999</Price>
                  <DiscountContainer>
                    <TextDiscount>99%</TextDiscount>
                  </DiscountContainer>
                </PriceProduct>
                <TitleLimitName>
                  Di jual kursi-kursian dengan meja-mejaan bodo amat
                </TitleLimitName>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="ios-information-circle-outline"
                      size={14}
                      color="#F3251D"
                      />
                    </IconCircle>
                    <PromotionText>
                      Lowest Price
                    </PromotionText>
                  </Promotion>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="logo-dropbox"
                      size={16}
                      color="#757886"
                      />
                    </IconCircle>
                    <PromotionDelivery>
                      Someday Delivery
                    </PromotionDelivery>
                  </Promotion>
              </ProductContainer>
          </WidthFullSize>
          
          <ButtonSecondaryInverseM>
              <ButtonSecondaryInverseMText>
                Lihat Semua (380+ Items)
              </ButtonSecondaryInverseMText>
            </ButtonSecondaryInverseM>  
        </Card>
        </MarginTopXXS>  */}
 
        <MarginTopS>
        <Image
          style={{...Platform.select({
            ios: {         
              height:height/3.6
            },
            android: {
              height:200
            },
            backgroundColor:'red'
          }),}}
          source={{uri: 'https://res.cloudinary.com/ruparupa-com/image/upload/f_auto,fl_lossy,w_640/v1535357785/category-inspirations/patterned-classy-thumbnail.jpg'}}
        />
        <InspirationBGCard>
        <FontSizeL><Bold><FontWhite>Patterned & Classy</FontWhite></Bold></FontSizeL>
        </InspirationBGCard>
        <Card>
        <View style={{marginTop:5}}>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
            >
              
            <View style={{ width:150, marginLeft:11,  borderRadius:3, backgroundColor:'#FFFFFF'}}>
                <View style={{flex:3}}>
                <Image source={{uri: 'https://res.cloudinary.com/ruparupa-com/image/upload/w_360,h_360,f_auto/v1507054880/Products/10120528_1.jpg'}}
                style={{flex:1, width:null, height:null, resizeMode:'cover', height:150}}
                />
                </View>
                <View style={{flex:3, padding:5, marginTop:5}}>
                <PriceProduct>
                  <Priceold>
                    <FontSizeXS>Rp 99.999.999</FontSizeXS>
                  </Priceold>
                  <Price>Rp 12.999.999</Price>
                  <DiscountContainer>
                    <TextDiscount>99%</TextDiscount>
                  </DiscountContainer>
                </PriceProduct>
                <TitleLimitName>
                  Di jual kursi-kursian dengan meja-mejaan bodo amat
                </TitleLimitName>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="ios-information-circle-outline"
                      size={14}
                      color="#F3251D"
                      />
                    </IconCircle>
                    <PromotionText>
                      Lowest Price
                    </PromotionText>
                  </Promotion>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="logo-dropbox"
                      size={16}
                      color="#757886"
                      />
                    </IconCircle>
                    <PromotionDelivery>
                      Someday Delivery
                    </PromotionDelivery>
                  </Promotion>
                </View>  
            </View>  

            <View style={{ width:150, marginLeft:11,  borderRadius:3, backgroundColor:'#FFFFFF'}}>
                <View style={{flex:3}}>
                <Image source={{uri: 'https://res.cloudinary.com/ruparupa-com/image/upload/w_360,h_360,f_auto/v1507054880/Products/10120528_1.jpg'}}
                style={{flex:1, width:null, height:null, resizeMode:'cover', height:150}}
                />
                </View>
                <View style={{flex:3, padding:5, marginTop:5}}>
                <PriceProduct>
                  <Priceold>
                    <FontSizeXS>Rp 99.999.999</FontSizeXS>
                  </Priceold>
                  <Price>Rp 12.999.999</Price>
                  <DiscountContainer>
                    <TextDiscount>99%</TextDiscount>
                  </DiscountContainer>
                </PriceProduct>
                <TitleLimitName>
                  Di jual kursi-kursian dengan meja-mejaan bodo amat
                </TitleLimitName>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="ios-information-circle-outline"
                      size={14}
                      color="#F3251D"
                      />
                    </IconCircle>
                    <PromotionText>
                      Lowest Price
                    </PromotionText>
                  </Promotion>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="logo-dropbox"
                      size={16}
                      color="#757886"
                      />
                    </IconCircle>
                    <PromotionDelivery>
                      Someday Delivery
                    </PromotionDelivery>
                  </Promotion>
                </View>  
            </View>  

            <View style={{ width:150, marginLeft:11,  borderRadius:3, backgroundColor:'#FFFFFF'}}>
                <View style={{flex:3}}>
                <Image source={{uri: 'https://res.cloudinary.com/ruparupa-com/image/upload/w_360,h_360,f_auto/v1507054880/Products/10120528_1.jpg'}}
                style={{flex:1, width:null, height:null, resizeMode:'cover', height:150}}
                />
                </View>
                <View style={{flex:3, padding:5, marginTop:5}}>
                <PriceProduct>
                  <Priceold>
                    <FontSizeXS>Rp 99.999.999</FontSizeXS>
                  </Priceold>
                  <Price>Rp 12.999.999</Price>
                  <DiscountContainer>
                    <TextDiscount>99%</TextDiscount>
                  </DiscountContainer>
                </PriceProduct>
                <TitleLimitName>
                  Di jual kursi-kursian dengan meja-mejaan bodo amat
                </TitleLimitName>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="ios-information-circle-outline"
                      size={14}
                      color="#F3251D"
                      />
                    </IconCircle>
                    <PromotionText>
                      Lowest Price
                    </PromotionText>
                  </Promotion>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="logo-dropbox"
                      size={16}
                      color="#757886"
                      />
                    </IconCircle>
                    <PromotionDelivery>
                      Someday Delivery
                    </PromotionDelivery>
                  </Promotion>
                </View>  
            </View>  

            <View style={{ width:150, marginLeft:11,  borderRadius:3, backgroundColor:'#FFFFFF'}}>
                <View style={{flex:3}}>
                <Image source={{uri: 'https://res.cloudinary.com/ruparupa-com/image/upload/w_360,h_360,f_auto/v1507054880/Products/10120528_1.jpg'}}
                style={{flex:1, width:null, height:null, resizeMode:'cover', height:150}}
                />
                </View>
                <View style={{flex:3, padding:5, marginTop:5}}>
                <PriceProduct>
                  <Priceold>
                    <FontSizeXS>Rp 99.999.999</FontSizeXS>
                  </Priceold>
                  <Price>Rp 12.999.999</Price>
                  <DiscountContainer>
                    <TextDiscount>99%</TextDiscount>
                  </DiscountContainer>
                </PriceProduct>
                <TitleLimitName>
                  Di jual kursi-kursian dengan meja-mejaan bodo amat
                </TitleLimitName>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="ios-information-circle-outline"
                      size={14}
                      color="#F3251D"
                      />
                    </IconCircle>
                    <PromotionText>
                      Lowest Price
                    </PromotionText>
                  </Promotion>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="logo-dropbox"
                      size={16}
                      color="#757886"
                      />
                    </IconCircle>
                    <PromotionDelivery>
                      Someday Delivery
                    </PromotionDelivery>
                  </Promotion>
                </View>  
            </View>  

            <View style={{ width:150, marginLeft:11,  borderRadius:3, backgroundColor:'#FFFFFF'}}>
                <View style={{flex:3}}>
                <Image source={{uri: 'https://res.cloudinary.com/ruparupa-com/image/upload/w_360,h_360,f_auto/v1507054880/Products/10120528_1.jpg'}}
                style={{flex:1, width:null, height:null, resizeMode:'cover', height:150}}
                />
                </View>
                <View style={{flex:3, padding:5, marginTop:5}}>
                <PriceProduct>
                  <Priceold>
                    <FontSizeXS>Rp 99.999.999</FontSizeXS>
                  </Priceold>
                  <Price>Rp 12.999.999</Price>
                  <DiscountContainer>
                    <TextDiscount>99%</TextDiscount>
                  </DiscountContainer>
                </PriceProduct>
                <TitleLimitName>
                  Di jual kursi-kursian dengan meja-mejaan bodo amat
                </TitleLimitName>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="ios-information-circle-outline"
                      size={14}
                      color="#F3251D"
                      />
                    </IconCircle>
                    <PromotionText>
                      Lowest Price
                    </PromotionText>
                  </Promotion>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="logo-dropbox"
                      size={16}
                      color="#757886"
                      />
                    </IconCircle>
                    <PromotionDelivery>
                      Someday Delivery
                    </PromotionDelivery>
                  </Promotion>
                </View>  
            </View>  

            </ScrollView>
          </View> 
          <Container>
          <ButtonSecondaryInverseM>
              <ButtonSecondaryInverseMText>
                Lihat Semua (380+ Items)
              </ButtonSecondaryInverseMText>
            </ButtonSecondaryInverseM>
          </Container>    
        </Card>
        </MarginTopS>



        <MarginTopS>
         <Swiper
            style={styles.wrapper}
            autoplay={true}
            loop={true}>
            <View style={styles.slide}>
                <Image style={styles.image} source={require('../img/banner1.jpg')} />
            </View>
            <View style={styles.slide}>
                <Image style={styles.image} source={require('../img/banner2.jpg')} />
            </View>
            <View style={styles.slide}>
                <Image style={styles.image} source={require('../img/banner3.jpg')} />
            </View>
            <View style={styles.slide}>
                <Image style={styles.image} source={require('../img/banner4.jpg')} />
            </View>
            <View style={styles.slide}>
                <Image style={styles.image} source={require('../img/banner5.jpg')} />
            </View>
          
        </Swiper>
        </MarginTopS>
        
          
        </ScrollView>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            alert('Modal has been closed.');
          }}>
          <SafeAreaView style={{ flex:1 }}>
          <View style={{marginTop: 20}}>
            <DistributeNyampingUjung>
              <View style={{marginLeft:10}}>
              <FontSizeXL><B>Syarat & Ketentuan</B></FontSizeXL>
              </View>
              <View style={{marginRight:10}}>
              <TouchableOpacity
                onPress={() => {
                  this.setModalVisible(!this.state.modalVisible);
                }}>
              <Ionicons
                    name="ios-close-circle"
                    size={28}
                    color="rgba(85, 87, 97, 1)"
                    style={{ alignSelf: "flex-end" }}
                  />
              </TouchableOpacity>
              </View>
            </DistributeNyampingUjung>
            <HR />
            <Container>
            <P>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vulputate neque eros, tincidunt rutrum eros ultricies sit amet. Proin pretium nisl sed nisl consequat efficitur. Ut semper justo ac turpis blandit, ut iaculis ipsum rhoncus. Donec eleifend interdum viverra. Ut ex dolor, interdum in eleifend at, volutpat sed sapien. Vestibulum in arcu finibus sapien feugiat interdum vitae eget ex. Curabitur scelerisque posuere tellus id facilisis.</P>
            <BR />
            <P>Nunc vehicula eleifend metus, placerat vulputate est rutrum id. Fusce euismod gravida velit, sed egestas risus pretium sed. Quisque auctor condimentum commodo. Duis sit amet urna accumsan, rutrum erat sit amet, vulputate purus. Donec luctus scelerisque fringilla. Curabitur auctor consectetur posuere. Morbi feugiat mi nunc, at vestibulum justo pulvinar at. Proin nec eleifend enim. Ut interdum tellus ut erat vehicula, id laoreet enim consequat. Aenean nisi nisl, pulvinar vel porta a, placerat id metus. Cras varius placerat fermentum. Nullam sapien mauris, mattis et semper vel, suscipit ac orci. In ut convallis tortor, eget gravida odio. Suspendisse mattis est et turpis posuere sagittis.</P>
            </Container>
          </View>
          </SafeAreaView>
        </Modal>
      </Bg>
    );
  }
}




export default HomePage;
