import React from "react";
import {
  View,
  Text,
  ScrollView,
  Button,
  TouchableOpacity,
  TextInput,
  Picker,
  Animated,
  StyleSheet
} from "react-native";
import { Ionicons , EvilIcons , FontAwesome } from "@expo/vector-icons";
import Image from "react-native-remote-svg";
import { 
  Bg,
  Container,
  BgText,
  Card,
  CardHeader,
  CardSubHeader,
  FormMainSearch,
  TextMainSearch,
  FormSecondarySearch,
  TextSecondarySearch,
  TextOrange,
  TextBlue,
  TextGray,
  BtnMainSearch,
  CardTitle,
  H1,
  H2,
  H3,
  H4,
  P,
  B,
  HR,
  FontSizeXXXL,
  FontSizeXXL,
  FontSizeXL,
  FontSizeL,
  FontSizeM,
  FontSizeS,
  FontSizeXS,
  FontSizeXXS,
  MarginXL,
  MarginL,
  MarginM,
  MarginS,
  MarginXS,
  MarginXXS,
  MarginTopXL,
  MarginTopL,
  MarginTopS,
  MarginTopXS,
  MarginTopXXS,
  MarginBottomXL,
  MarginBottomL,
  MarginBottomS,
  MarginBottomXS,
  MarginBottomXXS,
  MarginLeftXL,
  MarginLeftL,
  MarginLeftS,
  MarginLeftXS,
  MarginLeftXXS,
  MarginRightXL,
  MarginRightL,
  MarginRightS,
  MarginRightXS,
  MarginRightXXS,
  PaddingXL,
  PaddingL,
  PaddingM,
  PaddingS,
  PaddingXS,
  PaddingXXS,
  PaddingTopXL,
  PaddingTopL,
  PaddingTopS,
  PaddingTopXS,
  PaddingTopXXS,
  PaddingBottomXL,
  PaddingBottomL,
  PaddingBottomS,
  PaddingBottomXS,
  PaddingBottomXXS,
  PaddingLeftXL,
  PaddingLeftL,
  PaddingLeftS,
  PaddingLeftXS,
  PaddingLeftXXS,
  PaddingRightXL,
  PaddingRightL,
  PaddingRightS,
  PaddingRightXS,
  PaddingRightXXS,
  ButtonPrimaryM,
  ButtonPrimaryMDisabled,
  ButtonPrimaryMText,
  ButtonPrimaryInverseM,
  ButtonPrimaryInverseMText,
  ButtonGrayInverseM,
  ButtonGrayInverseMText,
  ButtonSecondaryM,
  ButtonSecondaryMDisabled,
  ButtonSecondaryMText,
  ButtonSecondaryInverseM,
  ButtonSecondaryInverseMText,
  Left,
  Center,
  Right,
  DistributeNyampingUjung,
  DistributeNyampingTengah,
  HeaderPills,
  FormM,
  FormMIcon,
  LabelForm,
  LeftM,
  InfoBoxPcp,
  NotifBadge,
  NotifText,
  ProductContainer,
  WidthFullSize,
  Priceold,
  Priceoldhr,
  PriceProduct,
  Price,
  TitleLimitName,
  DiscountContainer,
  TextDiscount,
  Wishlist,
  Promotion,
  IconCircle,
  PromotionText } from '../style/ruparupaUI'

class Dashboard extends React.Component {

  static navigationOptions = {
    header: null,
    drawerIcon: ({tintColor}) => (
      <Ionicons
                name="ios-contact"
                size={24}
                color="rgba(85, 87, 97, 1)"
                style={{color:tintColor}}
              />
    )
  }

  render() {
    return (
      <Bg>
        <CardHeader>
            <DistributeNyampingUjung>
            <TouchableOpacity onPress={()=>this.props.navigation.openDrawer()}>
                <Ionicons
                  name="md-menu"
                  size={32}
                  color="rgba(85, 87, 97, 1)"
                  style={{ marginLeft: 5 }}
                />
              </TouchableOpacity>
              <TouchableOpacity>
                <FormSecondarySearch>
                  <Ionicons
                    name="md-search"
                    size={18}
                    color="rgba(85, 87, 97, 1)"
                    style={{ alignSelf: "center" }}
                  />
                  <TextSecondarySearch>Cari di <TextGray>rup</TextGray><TextBlue>a</TextBlue><TextGray>rup</TextGray><TextOrange>a</TextOrange> </TextSecondarySearch>
                </FormSecondarySearch>
              </TouchableOpacity>
              <TouchableOpacity>
                <Ionicons
                  name="md-cart"
                  size={32}
                  color="rgba(85, 87, 97, 1)"
                  style={{ marginRight: 5 }}
                />
              </TouchableOpacity>
            </DistributeNyampingUjung>
          </CardHeader>
          <CardSubHeader>
              <HeaderPills>
                <Ionicons
                  name="ios-funnel-outline"
                  size={14}
                  color="rgba(85, 87, 97, 1)"
                  style={{}}
                /><LeftM>
                  <Center><FontSizeXS>Filter</FontSizeXS></Center>
                </LeftM>
              </HeaderPills>
              <HeaderPills>
                <Ionicons
                  name="ios-list"
                  size={16}
                  color="rgba(85, 87, 97, 1)"
                  style={{alignSelf:'center'}}
                /><LeftM>
                  <FontSizeXS>Urut Berdasarkan</FontSizeXS>
                </LeftM>
              </HeaderPills>
          </CardSubHeader>
        <ScrollView>
          <Text>fdafdafdafds</Text>
        </ScrollView>
      </Bg>
    );
  }
}

export default Dashboard;
