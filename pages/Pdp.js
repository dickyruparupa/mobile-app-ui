import React from "react";
import {
  View,
  Text,
  ScrollView,
  Button,
  TouchableOpacity,
  TextInput,
  Picker,
  Animated,
  StyleSheet,
  Dimensions
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import Image from "react-native-remote-svg";
import Swiper from 'react-native-swiper';
const { width } = Dimensions.get('window');

import { 
  Bg,
  Container,
  BgText,
  Card,
  CardHeader,
  CardSubHeader,
  FormMainSearch,
  TextMainSearch,
  FormSecondarySearch,
  TextSecondarySearch,
  TextOrange,
  TextBlue,
  TextGray,
  BtnMainSearch,
  CardTitle,
  H1,
  H2,
  H3,
  H4,
  P,
  B,
  HR,
  FontSizeXXXL,
  FontSizeXXL,
  FontSizeXL,
  FontSizeL,
  FontSizeM,
  FontSizeS,
  FontSizeXS,
  FontSizeXXS,
  MarginXL,
  MarginL,
  MarginM,
  MarginS,
  MarginXS,
  MarginXXS,
  MarginTopXL,
  MarginTopL,
  MarginTopM,
  MarginTopS,
  MarginTopXS,
  MarginTopXXS,
  MarginBottomXL,
  MarginBottomL,
  MarginBottomS,
  MarginBottomXS,
  MarginBottomXXS,
  MarginLeftXL,
  MarginLeftL,
  MarginLeftS,
  MarginLeftXS,
  MarginLeftXXS,
  MarginRightXL,
  MarginRightL,
  MarginRightS,
  MarginRightXS,
  MarginRightXXS,
  PaddingXL,
  PaddingL,
  PaddingM,
  PaddingS,
  PaddingXS,
  PaddingXXS,
  PaddingTopXL,
  PaddingTopL,
  PaddingTopS,
  PaddingTopXS,
  PaddingTopXXS,
  PaddingBottomXL,
  PaddingBottomL,
  PaddingBottomS,
  PaddingBottomXS,
  PaddingBottomXXS,
  PaddingLeftXL,
  PaddingLeftL,
  PaddingLeftS,
  PaddingLeftXS,
  PaddingLeftXXS,
  PaddingRightXL,
  PaddingRightL,
  PaddingRightS,
  PaddingRightXS,
  PaddingRightXXS,
  ButtonPrimaryM,
  ButtonPrimaryMDisabled,
  ButtonPrimaryMText,
  ButtonPrimaryInverseM,
  ButtonPrimaryInverseMText,
  ButtonGrayInverseM,
  ButtonGrayInverseMText,
  ButtonSecondaryM,
  ButtonSecondaryMDisabled,
  ButtonSecondaryMText,
  ButtonSecondaryInverseM,
  ButtonSecondaryInverseMText,
  Left,
  Center,
  Right,
  DistributeNyampingUjung,
  DistributeNyampingTengah,
  HeaderPills,
  FormM,
  FormMIcon,
  LabelForm,
  LeftM,
  InfoBoxPcp,
  NotifBadge,
  NotifText,
  ProductContainer,
  WidthFullSize,
  Priceold,
  Priceoldhr,
  PriceProduct,
  Price,
  TitleLimitName,
  DiscountContainer,
  TextDiscount,
  Wishlist,
  Promotion,
  IconCircle,
  PromotionText,
  BreadcrumbFont,
  Bold,
  DiscountContainerPDP,
  PricePDP,
  TextDiscountPDP,
  FontPrimary,
  FontSecondary,
  FontWarning, 
  FontPositive,
  FontNegative,
  StickyFooter,
 } from '../style/ruparupaUI'




class ProductDetailPage extends React.Component {

  static navigationOptions = {
    header: null,
    drawerIcon: ({tintColor}) => (
      <Ionicons
                name="md-american-football"
                size={24}
                color="rgba(85, 87, 97, 1)"
                style={{color:tintColor}}
              />
    )
  }

  render() {
    return (
      <Bg>
        <CardHeader>
            <DistributeNyampingUjung>
            <TouchableOpacity onPress={()=>this.props.navigation.openDrawer()}>
                <Ionicons
                  name="md-menu"
                  size={32}
                  color="rgba(85, 87, 97, 1)"
                  style={{ marginLeft: 5 }}
                />
              </TouchableOpacity>
              <TouchableOpacity>
                <FormSecondarySearch>
                  <Ionicons
                    name="md-search"
                    size={18}
                    color="rgba(85, 87, 97, 1)"
                    style={{ alignSelf: "center" }}
                  />
                  <TextSecondarySearch>Cari di <TextGray>rup</TextGray><TextBlue>a</TextBlue><TextGray>rup</TextGray><TextOrange>a</TextOrange> </TextSecondarySearch>
                </FormSecondarySearch>
              </TouchableOpacity>
              <TouchableOpacity>
                <Ionicons
                  name="md-cart"
                  size={32}
                  color="rgba(85, 87, 97, 1)"
                  style={{ marginRight: 5 }}
                />
              </TouchableOpacity>
            </DistributeNyampingUjung>
          </CardHeader>
          
          <ScrollView>
            <Swiper
            style={styles.wrapper}
            renderPagination={renderPagination}
            loop={true}>
            <View style={styles.slide} title={<Text numberOfLines={1}></Text>}>
                <Image style={styles.image} source={require('../img/1.jpg')} />
            </View>
            <View style={styles.slide} title={<Text numberOfLines={1}></Text>}>
                <Image style={styles.image} source={require('../img/2.jpg')}/>
            </View>
            <View style={styles.slide} title={<Text numberOfLines={1}></Text>}>
                <Image style={styles.image} source={require('../img/3.jpg')}/>
            </View>
            <View style={styles.slide} title={<Text numberOfLines={1}></Text>}>
                <Image style={styles.image} source={require('../img/4.jpg')}/>
            </View>
            </Swiper>

            <MarginTopS>
                <Card>
                 <Container>   
                    <BreadcrumbFont>
                        Furnitur / Lemari / Lemari Kantor / Lemari Arsip
                    </BreadcrumbFont>   
                    <MarginTopM>
                        <FontSizeL>
                            <Bold>    
                                Lemari Arsip Roda 1 Pintu - Putih
                            </Bold>
                        </FontSizeL>
                    </MarginTopM>
                    <MarginTopS>
                    <PriceProduct>
                        <Priceold>
                            <FontSizeXS>Rp 999.000</FontSizeXS>
                        </Priceold>                    
                        <PricePDP>Rp 699.000</PricePDP>
                        <DiscountContainerPDP>
                            <TextDiscountPDP>30%</TextDiscountPDP>
                        </DiscountContainerPDP>
                        </PriceProduct>  
                    </MarginTopS>
                    
                    <MarginTopL>
                        <FontSizeXS>
                            official store
                        </FontSizeXS>
                        <FontSizeM>
                        <Ionicons
                        name="ios-checkmark-circle-outline"
                        size={16}
                        color="rgba(85, 87, 97, 1)"
                        style={{ alignSelf: "flex-start" }}
                        /> INFORMA
                        </FontSizeM>
                        <FontSizeXS>
                            dikirim oleh
                        </FontSizeXS>
                        <FontSizeM>
                        <Ionicons
                        name="md-send"
                        size={16}
                        color="rgba(85, 87, 97, 1)"
                        style={{ alignSelf: "flex-start" }}
                        /> Ruparupa
                        </FontSizeM>       
                    </MarginTopL>

                    <MarginTopM>
                        <FontSizeM>
                        <Ionicons
                        name="ios-checkmark-circle"
                        size={16}
                        color="rgba(4, 147, 114, 1)"
                        style={{ alignSelf: "flex-start" }}
                        />
                        {/* <MarginRightXXS /> */}
                            <Bold>
                                <FontPositive>Stock tersedia untuk pengiriman</FontPositive>
                            </Bold>
                         </FontSizeM>       
                    </MarginTopM>

                    <MarginTopXXS>
                    <FontSizeXS>
                            <FontPositive>Ke Kembangan di Kota Jakarta Barat - DKI JAKARTA</FontPositive>
                         </FontSizeXS>
                    </MarginTopXXS>    

                    <MarginTopS>
                     <ButtonGrayInverseM>
                        <ButtonGrayInverseMText>
                            Ubah lokasi pengiriman
                        </ButtonGrayInverseMText>
                    </ButtonGrayInverseM>  
                    </MarginTopS> 

                    <MarginTopS>
                        <P>
                            <Bold>
                                Free Shipping
                            </Bold>
                        </P>
                        <FontSizeXXS>
                            JABODETABEK
                        </FontSizeXXS>
                         {/* <Image
                        source={{
                            uri:
                            "https://res.cloudinary.com/ruparupa-com/image/upload/v1524564956/2.1/svg/free-delivery.svg"
                        }}
                        style={{ height: 20, alignSelf: 'flex-start' }}
                        />            */}
                    </MarginTopS>

                    <HR />
                    <P>
                        webview disini
                    </P> 
                    <HR />  
                    <P>
                        Gambar SVG disini
                    </P>
                    <HR /> 

                    <MarginTopS>
                        <FontSizeL>
                            <Bold>
                                Cicilan 0%
                            </Bold>
                        </FontSizeL>
                        <MarginTopXS>
                            <FontSizeXS>3 Bulan 0% (Min Trx 500.000) - Rp 233.000/ bulan</FontSizeXS>
                        </MarginTopXS>
                        <MarginTopXS>
                            <FontSizeXS>6 Bulan 0% (Min Trx 1.000.000) - Rp 116.500/ bulan</FontSizeXS>
                        </MarginTopXS>
                        <MarginTopXS>
                            <FontSizeXS>12 Bulan 0% (Min Trx 3.000.000) - Rp 58.250/ bulan</FontSizeXS>
                        </MarginTopXS>        
                    </MarginTopS>

                    <MarginTopS>
                        <P>picker disini</P>
                    </MarginTopS>

                    <HR />  
                    <P>
                        Gambar SVG disini
                    </P>
                    <MarginTopS>
                
                    <FontSizeL>
                        <Bold>
                            Produk yang mungkin Anda suka
                        </Bold>
                    </FontSizeL>   
            </MarginTopS> 
            </Container>
                </Card>
                
            </MarginTopS>

          </ScrollView>  
          <StickyFooter>
          <ButtonPrimaryM>
              <ButtonPrimaryMText>Tambahkan ke keranjang</ButtonPrimaryMText>
            </ButtonPrimaryM>
          </StickyFooter>
      </Bg>
    );
  }
}




const styles = {
    wrapper: {
        height:360
    },
    slide: {
      flex: 1,
      justifyContent: 'center',
      backgroundColor: 'transparent'
    },
    image: {
      width,
      flex: 1
    },
    paginationStyle: {
      position: 'absolute',
      bottom: 10,
      right: 10,
    },
    paginationText: {
      color: '#555761',
      fontSize: 14
    }
  }
  

  const renderPagination = (index, total, context) => {
    return (
      <View style={styles.paginationStyle}>
        <Text style={{ color: '#555761' }}>
        <Ionicons
                  name="ios-image"
                  size={14}
                  color="rgba(85, 87, 97, 1)"
                  style={{ alignSelf: "center"}}
                />
          <Text style={styles.paginationText}> {index + 1}</Text>/{total}
        </Text>
      </View>
    )
  }
  

export default ProductDetailPage;
