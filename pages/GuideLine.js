import React from "react";
import {
  View,
  Text,
  ScrollView,
  Button,
  TouchableOpacity,
  TextInput,
  Picker,
  Animated,
  StyleSheet
} from "react-native";
import { Ionicons , EvilIcons , FontAwesome } from "@expo/vector-icons";
import Image from "react-native-remote-svg";
import { 
  Bg,
  Container,
  BgText,
  Card,
  CardHeader,
  CardSubHeader,
  FormMainSearch,
  TextMainSearch,
  FormSecondarySearch,
  TextSecondarySearch,
  TextOrange,
  TextBlue,
  TextGray,
  BtnMainSearch,
  CardTitle,
  H1,
  H2,
  H3,
  H4,
  P,
  B,
  HR,
  FontSizeXXXL,
  FontSizeXXL,
  FontSizeXL,
  FontSizeL,
  FontSizeM,
  FontSizeS,
  FontSizeXS,
  FontSizeXXS,
  MarginXL,
  MarginL,
  MarginM,
  MarginS,
  MarginXS,
  MarginXXS,
  MarginTopXL,
  MarginTopL,
  MarginTopS,
  MarginTopXS,
  MarginTopXXS,
  MarginBottomXL,
  MarginBottomL,
  MarginBottomS,
  MarginBottomXS,
  MarginBottomXXS,
  MarginLeftXL,
  MarginLeftL,
  MarginLeftS,
  MarginLeftXS,
  MarginLeftXXS,
  MarginRightXL,
  MarginRightL,
  MarginRightS,
  MarginRightXS,
  MarginRightXXS,
  PaddingXL,
  PaddingL,
  PaddingM,
  PaddingS,
  PaddingXS,
  PaddingXXS,
  PaddingTopXL,
  PaddingTopL,
  PaddingTopS,
  PaddingTopXS,
  PaddingTopXXS,
  PaddingBottomXL,
  PaddingBottomL,
  PaddingBottomS,
  PaddingBottomXS,
  PaddingBottomXXS,
  PaddingLeftXL,
  PaddingLeftL,
  PaddingLeftS,
  PaddingLeftXS,
  PaddingLeftXXS,
  PaddingRightXL,
  PaddingRightL,
  PaddingRightS,
  PaddingRightXS,
  PaddingRightXXS,
  ButtonPrimaryM,
  ButtonPrimaryMDisabled,
  ButtonPrimaryMText,
  ButtonPrimaryInverseM,
  ButtonPrimaryInverseMText,
  ButtonGrayInverseM,
  ButtonGrayInverseMText,
  ButtonSecondaryM,
  ButtonSecondaryMDisabled,
  ButtonSecondaryMText,
  ButtonSecondaryInverseM,
  ButtonSecondaryInverseMText,
  Left,
  Center,
  Right,
  DistributeNyampingUjung,
  DistributeNyampingTengah,
  HeaderPills,
  FormM,
  FormMIcon,
  LabelForm,
  LeftM,
  InfoBoxPcp,
  NotifBadge,
  NotifText,
  ProductContainer,
  WidthFullSize,
  Priceold,
  Priceoldhr,
  PriceProduct,
  Price,
  TitleLimitName,
  DiscountContainer,
  TextDiscount,
  Wishlist,
  Promotion,
  IconCircle,
  PromotionText,
  StickyFooter,
   } from '../style/ruparupaUI'

class GuideLine extends React.Component {

  static navigationOptions = {
    header: null,
    drawerIcon: ({tintColor}) => (
      <Ionicons
                name="ios-create-outline"
                size={24}
                color="rgba(85, 87, 97, 1)"
                style={{color:tintColor}}
              />
    )
  }

  render() {
    return (
      <Bg>
        <CardHeader>
          <DistributeNyampingUjung>
            <TouchableOpacity onPress={()=>this.props.navigation.openDrawer()}>
              <Ionicons
                name="md-menu"
                size={32}
                color="rgba(85, 87, 97, 1)"
                style={{}}
              />
            </TouchableOpacity>
            <Image
              source={{
                uri:
                  "https://res.cloudinary.com/ruparupa-com/image/upload/v1524564956/ruparupa-assets/img/ruparupa-logo.svg"
              }}
              style={{ height: 25, alignSelf: 'center' }}
            />
            <TouchableOpacity>
              <Ionicons
                name="md-cart"
                size={32}
                color="rgba(85, 87, 97, 1)"
                style={{}}
              />
            </TouchableOpacity>
          </DistributeNyampingUjung>
          <TouchableOpacity>
            <FormMainSearch>
              <DistributeNyampingUjung>
                <TextMainSearch>Cari ruparupa produk atau merek</TextMainSearch>
                <BtnMainSearch>
                  <Ionicons
                    name="md-search"
                    size={22}
                    color="rgba(255, 255, 255, 1)"
                    style={{ alignSelf: "center" }}
                  />
                </BtnMainSearch>
              </DistributeNyampingUjung>
            </FormMainSearch>
          </TouchableOpacity>
        </CardHeader>
        <ScrollView>
          <CardTitle>Header Selain Index</CardTitle>
          <CardHeader>
            <DistributeNyampingUjung>
              <TouchableOpacity>
                <Ionicons
                  name="md-menu"
                  size={32}
                  color="rgba(85, 87, 97, 1)"
                  style={{ marginLeft: 5 }}
                />
              </TouchableOpacity>
              <TouchableOpacity>
                <FormSecondarySearch>
                  <Ionicons
                    name="md-search"
                    size={18}
                    color="rgba(85, 87, 97, 1)"
                    style={{ alignSelf: "center" }}
                  />
                  <TextSecondarySearch>Cari di <TextGray>rup</TextGray><TextBlue>a</TextBlue><TextGray>rup</TextGray><TextOrange>a</TextOrange> </TextSecondarySearch>
                </FormSecondarySearch>
              </TouchableOpacity>
              <TouchableOpacity>
                <Ionicons
                  name="md-cart"
                  size={32}
                  color="rgba(85, 87, 97, 1)"
                  style={{ marginRight: 5 }}
                />
              </TouchableOpacity>
            </DistributeNyampingUjung>
          </CardHeader>
          <CardTitle>Header Selain Index PCP</CardTitle>
          <CardHeader>
            <DistributeNyampingUjung>
              <TouchableOpacity>
                <Ionicons
                  name="md-menu"
                  size={32}
                  color="rgba(85, 87, 97, 1)"
                  style={{ marginLeft: 5 }}
                />
              </TouchableOpacity>
              <TouchableOpacity>
                <FormSecondarySearch>
                  <Ionicons
                    name="md-search"
                    size={18}
                    color="rgba(85, 87, 97, 1)"
                    style={{ alignSelf: "center" }}
                  />
                  <TextSecondarySearch>Cari di <TextGray>rup</TextGray><TextBlue>a</TextBlue><TextGray>rup</TextGray><TextOrange>a</TextOrange> </TextSecondarySearch>
                </FormSecondarySearch>
              </TouchableOpacity>
              <TouchableOpacity>
                <Ionicons
                  name="md-cart"
                  size={32}
                  color="rgba(85, 87, 97, 1)"
                  style={{ marginRight: 5 }}
                />
              </TouchableOpacity>
            </DistributeNyampingUjung>
          </CardHeader>
          <CardSubHeader>
              <HeaderPills>
                <Ionicons
                  name="ios-funnel-outline"
                  size={14}
                  color="rgba(85, 87, 97, 1)"
                  style={{}}
                /><LeftM>
                  <Center><FontSizeXS>Filter</FontSizeXS></Center>
                </LeftM>
              </HeaderPills>
              <HeaderPills>
                <Ionicons
                  name="ios-list"
                  size={16}
                  color="rgba(85, 87, 97, 1)"
                  style={{alignSelf:'center'}}
                /><LeftM>
                  <FontSizeXS>Urut Berdasarkan</FontSizeXS>
                </LeftM>
              </HeaderPills>
          </CardSubHeader>
          
          <CardTitle>Info box</CardTitle>
          <InfoBoxPcp>
            <Ionicons
              name="ios-information-circle"
              size={16}
              color="rgba(85, 87, 97, 1)"
              style={{ alignSelf: "flex-start" }}
            />
            <LeftM>
              <FontSizeS>Bacod goes here</FontSizeS>
            </LeftM>
          </InfoBoxPcp>
          <Container>
            <CardTitle>Input</CardTitle>
            <Card>
              <Container>
              <LabelForm>Label Form</LabelForm>
              <FormM>
                <TextInput
                  placeholder="Placeholder text"
                  underlineColorAndroid="transparent"
                  selectionColor="rgba(242, 101, 37, 1)"
                  keyboardType="default"
                />
              </FormM>
              <LabelForm>Form with Icon</LabelForm>
              <FormMIcon>
                <Ionicons
                  name="md-search"
                  size={18}
                  color="rgba(85, 87, 97, 1)"
                  style={{ alignSelf: "center" }}
                />
                <LeftM>
                  <TextInput
                    placeholder="Placeholder text"
                    underlineColorAndroid="transparent"
                    selectionColor="rgba(242, 101, 37, 1)"
                    keyboardType="default"
                    style={{ width: 300 }}
                  />
                </LeftM>
              </FormMIcon>
              <LabelForm>Numeric Form</LabelForm>
              <FormM>
                <TextInput
                  placeholder="Placeholder text"
                  underlineColorAndroid="transparent"
                  selectionColor="rgba(242, 101, 37, 1)"
                  keyboardType="numeric"
                />
              </FormM>
              </Container>
            </Card>
            <CardTitle>Layout</CardTitle>
            <Left>
              <FontSizeL>Kiri</FontSizeL>
            </Left>
            <Center>
              <FontSizeL>Tengah</FontSizeL>
            </Center>
            <Right>
              <FontSizeL>Kanan</FontSizeL>
            </Right>
            <HR />
            <DistributeNyampingUjung>
              <FontSizeM>Satu</FontSizeM>
              <FontSizeM>Dua</FontSizeM>
              <FontSizeM>Tiga</FontSizeM>
            </DistributeNyampingUjung>
            <CardTitle>Buttons</CardTitle>
            <Container>
            <ButtonPrimaryM onPress={() => this.props.navigation.navigate('Pcp')}>
              <ButtonPrimaryMText>Ke PCP</ButtonPrimaryMText>
            </ButtonPrimaryM>
            <ButtonPrimaryInverseM>
              <ButtonPrimaryInverseMText>
                Primary Inverse Button
              </ButtonPrimaryInverseMText>
            </ButtonPrimaryInverseM>
            <ButtonSecondaryM>
              <ButtonSecondaryMText>Secondary Button</ButtonSecondaryMText>
            </ButtonSecondaryM>
            <ButtonSecondaryInverseM>
              <ButtonSecondaryInverseMText>
                Secondary Inverse Button
              </ButtonSecondaryInverseMText>
            </ButtonSecondaryInverseM>
            <ButtonGrayInverseM>
              <ButtonGrayInverseMText>
                Gray Inverse Button
              </ButtonGrayInverseMText>
            </ButtonGrayInverseM>
            <ButtonPrimaryMDisabled disabled={true}>
              <ButtonPrimaryMText>Primary Button</ButtonPrimaryMText>
            </ButtonPrimaryMDisabled>
            <ButtonSecondaryMDisabled disabled={true}>
              <ButtonPrimaryMText>Secondary Button</ButtonPrimaryMText>
            </ButtonSecondaryMDisabled>
            </Container>
            <CardTitle>Heading</CardTitle>
            <Card>
              <Container>
              <H2>
                <B>(H2)</B> The quick brown fox jumps over the lazy dog
              </H2>
              <HR />
              <H3>
                <B>(H3)</B> The quick brown fox jumps over the lazy dog
              </H3>
              <HR />
              <H4>
                <B>(H4)</B> The quick brown fox jumps over the lazy dog
              </H4>
              <HR />
              <P>
                <B>(P)</B> The quick brown fox jumps over the lazy dog
              </P>
              </Container>
            </Card>
            <CardTitle>Font Sizes</CardTitle>
            <Card>
              <Container>
              <FontSizeXL>
                <B>(XL)</B> The quick brown fox jumps over the lazy dog
              </FontSizeXL>
              <HR />
              <FontSizeL>
                <B>(L)</B> The quick brown fox jumps over the lazy dog
              </FontSizeL>
              <HR />
              <FontSizeM>
                <B>(M)</B> The quick brown fox jumps over the lazy dog
              </FontSizeM>
              <HR />
              <FontSizeS>
                <B>(S)</B> The quick brown fox jumps over the lazy dog
              </FontSizeS>
              <HR />
              <FontSizeXS>
                <B>(XS)</B> The quick brown fox jumps over the lazy dog
              </FontSizeXS>
              <HR />
              <FontSizeXXS>
                <B>(XXS)</B> The quick brown fox jumps over the lazy dog
              </FontSizeXXS>
              </Container>
            </Card>           
          </Container>
        </ScrollView>
      </Bg>
    );
  }
}

export default GuideLine;
