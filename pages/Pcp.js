import React from "react";
import {
  View,
  Text,
  ScrollView,
  Button,
  TouchableOpacity,
  TextInput,
  Picker,
  Animated,
  StyleSheet,
  Modal,
  SafeAreaView,
  TouchableHighlight,
} from "react-native";
import { Ionicons , EvilIcons , FontAwesome , Feather } from "@expo/vector-icons";
import Image from "react-native-remote-svg";
import CheckBox from 'react-native-check-box';
import { Col, Row, Grid } from "react-native-easy-grid";
import { 
  Bg,
  Container,
  BgText,
  Card,
  CardHeader,
  CardSubHeader,
  FormMainSearch,
  TextMainSearch,
  FormSecondarySearch,
  TextSecondarySearch,
  TextOrange,
  TextBlue,
  TextGray,
  BtnMainSearch,
  CardTitle,
  H1,
  H2,
  H3,
  H4,
  P,
  B,
  HR,
  FontSizeXXXL,
  FontSizeXXL,
  FontSizeXL,
  FontSizeL,
  FontSizeM,
  FontSizeS,
  FontSizeXS,
  FontSizeXXS,
  BR,
  MarginXL,
  MarginL,
  MarginM,
  MarginS,
  MarginXS,
  MarginXXS,
  MarginTopXL,
  MarginTopL,
  MarginTopS,
  MarginTopXS,
  MarginTopXXS,
  MarginBottomXL,
  MarginBottomL,
  MarginBottomS,
  MarginBottomXS,
  MarginBottomXXS,
  MarginLeftXL,
  MarginLeftL,
  MarginLeftS,
  MarginLeftXS,
  MarginLeftXXS,
  MarginRightXL,
  MarginRightL,
  MarginRightS,
  MarginRightXS,
  MarginRightXXS,
  PaddingXL,
  PaddingL,
  PaddingM,
  PaddingS,
  PaddingXS,
  PaddingXXS,
  PaddingTopXL,
  PaddingTopL,
  PaddingTopS,
  PaddingTopXS,
  PaddingTopXXS,
  PaddingBottomXL,
  PaddingBottomL,
  PaddingBottomS,
  PaddingBottomXS,
  PaddingBottomXXS,
  PaddingLeftXL,
  PaddingLeftL,
  PaddingLeftS,
  PaddingLeftXS,
  PaddingLeftXXS,
  PaddingRightXL,
  PaddingRightL,
  PaddingRightS,
  PaddingRightXS,
  PaddingRightXXS,
  ButtonPrimaryM,
  ButtonPrimaryMDisabled,
  ButtonPrimaryMText,
  ButtonPrimaryInverseM,
  ButtonPrimaryInverseMText,
  ButtonGrayInverseM,
  ButtonGrayInverseMText,
  ButtonSecondaryM,
  ButtonSecondaryMDisabled,
  ButtonSecondaryMText,
  ButtonSecondaryInverseM,
  ButtonSecondaryInverseMText,
  Left,
  Center,
  Right,
  DistributeNyampingUjung,
  DistributeNyampingTengah,
  HeaderPills,
  FormM,
  FormMIcon,
  LabelForm,
  LeftM,
  InfoBoxPcp,
  NotifBadge,
  NotifText,
  ProductContainer,
  WidthFullSize,
  Priceold,
  Priceoldhr,
  PriceProduct,
  Price,
  TitleLimitName,
  DiscountContainer,
  TextDiscount,
  Wishlist,
  Promotion,
  IconCircle,
  PromotionText,
  PromotionDelivery,
  CardList
 } from '../style/ruparupaUI'

class ProductCategoryPage extends React.Component {

  static navigationOptions = {
    header: null,
    drawerIcon: ({tintColor}) => (
      <Ionicons
                name="md-list"
                size={24}
                color="rgba(85, 87, 97, 1)"
                style={{color:tintColor}}
              />
    )
  }

  state = {
    modalVisible: false,
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  render() {
    return (
      <Bg>
        <CardHeader>
            <DistributeNyampingUjung>
            <TouchableOpacity onPress={()=>this.props.navigation.openDrawer()}>
                <Ionicons
                  name="md-menu"
                  size={32}
                  color="rgba(85, 87, 97, 1)"
                  style={{ marginLeft: 5 }}
                />
              </TouchableOpacity>
              <TouchableOpacity>
                <FormSecondarySearch>
                  <Ionicons
                    name="md-search"
                    size={18}
                    color="rgba(85, 87, 97, 1)"
                    style={{ alignSelf: "center" }}
                  />
                  <TextSecondarySearch>Cari di <TextGray>rup</TextGray><TextBlue>a</TextBlue><TextGray>rup</TextGray><TextOrange>a</TextOrange> </TextSecondarySearch>
                </FormSecondarySearch>
              </TouchableOpacity>
              <TouchableOpacity>
                <Ionicons
                  name="md-cart"
                  size={32}
                  color="rgba(85, 87, 97, 1)"
                  style={{ marginRight: 5 }}
                />
              </TouchableOpacity>
            </DistributeNyampingUjung>
          </CardHeader>
          <CardSubHeader>
          
              <HeaderPills
               onPress={() => {
                this.setModalVisible(true);
              }}>
                <Ionicons
                  name="ios-funnel-outline"
                  size={14}
                  color="rgba(85, 87, 97, 1)"
                  style={{}}
                /><LeftM>
                  <Center><FontSizeXS>Filter</FontSizeXS></Center>
                </LeftM>
              </HeaderPills>
              <HeaderPills>
                <Ionicons
                  name="ios-list"
                  size={16}
                  color="rgba(85, 87, 97, 1)"
                  style={{alignSelf:'center'}}
                /><LeftM>
                  <FontSizeXS>Urut Berdasarkan</FontSizeXS>
                </LeftM>
              </HeaderPills>
          </CardSubHeader>
          <ScrollView>
          <InfoBoxPcp>
            <Ionicons
              name="ios-information-circle"
              size={16}
              color="rgba(85, 87, 97, 1)"
              style={{ alignSelf: "flex-start" }}
            />
            <LeftM>
              <FontSizeS>Bacod goes here</FontSizeS>
            </LeftM>
          </InfoBoxPcp>
          <Card>
          <WidthFullSize>
          <ProductContainer>
                <TouchableOpacity>
                <Right>
                  <Wishlist>
                    <EvilIcons name="heart" size={24} color="rgba(85, 87, 97, 1)" style={{ alignSelf: "flex-start" }} />
                  </Wishlist>
                </Right>
                </TouchableOpacity>
                <TouchableOpacity>
                <Image
                  source={{
                    uri:
                      "https://res.cloudinary.com/ruparupa-com/image/upload/w_170,h_170,f_auto/v1510165192/Products/10119801_1.jpg"
                  }}
                  style={{ flex:1, height: 150 }}
                  resizeMode="contain"
                />
                <PriceProduct>
                  <Priceold>
                    <FontSizeXS>Rp 99.999.999</FontSizeXS>
                  </Priceold>
                  <Price>Rp 12.999.999</Price>
                  <DiscountContainer>
                    <TextDiscount>99%</TextDiscount>
                  </DiscountContainer>
                </PriceProduct>
                <TitleLimitName>
                  Di jual kursi-kursian dengan meja-mejaan bodo amat
                </TitleLimitName>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="ios-information-circle-outline"
                      size={14}
                      color="#F3251D"
                      />
                    </IconCircle>
                    <PromotionText>
                      Lowest Price
                    </PromotionText>
                  </Promotion>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="logo-dropbox"
                      size={16}
                      color="#757886"
                      />
                    </IconCircle>
                    <PromotionDelivery>
                      Someday Delivery
                    </PromotionDelivery>
                  </Promotion>
                  </TouchableOpacity>
              </ProductContainer>
              <ProductContainer>
                <TouchableOpacity>
                <Right>
                  <Wishlist>
                    <EvilIcons name="heart" size={24} color="rgba(85, 87, 97, 1)" style={{ alignSelf: "flex-start" }} />
                  </Wishlist>
                </Right>
                </TouchableOpacity>
                <TouchableOpacity>
                <Image
                  source={{
                    uri:
                      "https://res.cloudinary.com/ruparupa-com/image/upload/w_170,h_170,f_auto/v1510165192/Products/10119801_1.jpg"
                  }}
                  style={{ flex:1, height: 150 }}
                  resizeMode="contain"
                />
                <PriceProduct>
                  <Priceold>
                    <FontSizeXS>Rp 99.999.999</FontSizeXS>
                  </Priceold>
                  <Price>Rp 12.999.999</Price>
                  <DiscountContainer>
                    <TextDiscount>99%</TextDiscount>
                  </DiscountContainer>
                </PriceProduct>
                <TitleLimitName>
                  Di jual kursi-kursian dengan meja-mejaan bodo amat
                </TitleLimitName>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="ios-information-circle-outline"
                      size={14}
                      color="#F3251D"
                      />
                    </IconCircle>
                    <PromotionText>
                      Lowest Price
                    </PromotionText>
                  </Promotion>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="logo-dropbox"
                      size={16}
                      color="#757886"
                      />
                    </IconCircle>
                    <PromotionDelivery>
                      Someday Delivery
                    </PromotionDelivery>
                  </Promotion>
                  </TouchableOpacity>
              </ProductContainer>
          </WidthFullSize>
          <WidthFullSize>
              <ProductContainer>
                <TouchableOpacity>
                <Right>
                  <Wishlist>
                    <EvilIcons name="heart" size={24} color="rgba(85, 87, 97, 1)" style={{ alignSelf: "flex-start" }} />
                  </Wishlist>
                </Right>
                </TouchableOpacity>
                <TouchableOpacity>
                <Image
                  source={{
                    uri:
                      "https://res.cloudinary.com/ruparupa-com/image/upload/w_170,h_170,f_auto/v1510165192/Products/10119801_1.jpg"
                  }}
                  style={{ flex:1, height: 150 }}
                  resizeMode="contain"
                />
                <PriceProduct>
                  <Priceold>
                    <FontSizeXS>Rp 99.999.999</FontSizeXS>
                  </Priceold>
                  <Price>Rp 12.999.999</Price>
                  <DiscountContainer>
                    <TextDiscount>99%</TextDiscount>
                  </DiscountContainer>
                </PriceProduct>
                <TitleLimitName>
                  Di jual kursi-kursian dengan meja-mejaan bodo amat
                </TitleLimitName>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="ios-information-circle-outline"
                      size={14}
                      color="#F3251D"
                      />
                    </IconCircle>
                    <PromotionText>
                      Lowest Price
                    </PromotionText>
                  </Promotion>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="logo-dropbox"
                      size={16}
                      color="#757886"
                      />
                    </IconCircle>
                    <PromotionDelivery>
                      Someday Delivery
                    </PromotionDelivery>
                  </Promotion>
                  </TouchableOpacity>
              </ProductContainer>
              <ProductContainer>
                <TouchableOpacity>
                <Right>
                  <Wishlist>
                    <EvilIcons name="heart" size={24} color="rgba(85, 87, 97, 1)" style={{ alignSelf: "flex-start" }} />
                  </Wishlist>
                </Right>
                </TouchableOpacity>
                <TouchableOpacity>
                <Image
                  source={{
                    uri:
                      "https://res.cloudinary.com/ruparupa-com/image/upload/w_170,h_170,f_auto/v1510165192/Products/10119801_1.jpg"
                  }}
                  style={{ flex:1, height: 150 }}
                  resizeMode="contain"
                />
                <PriceProduct>
                  <Priceold>
                    <FontSizeXS>Rp 99.999.999</FontSizeXS>
                  </Priceold>
                  <Price>Rp 12.999.999</Price>
                  <DiscountContainer>
                    <TextDiscount>99%</TextDiscount>
                  </DiscountContainer>
                </PriceProduct>
                <TitleLimitName>
                  Di jual kursi-kursian dengan meja-mejaan bodo amat
                </TitleLimitName>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="ios-information-circle-outline"
                      size={14}
                      color="#F3251D"
                      />
                    </IconCircle>
                    <PromotionText>
                      Lowest Price
                    </PromotionText>
                  </Promotion>
                  <Promotion>
                    <IconCircle>
                      <Ionicons
                      name="logo-dropbox"
                      size={16}
                      color="#757886"
                      />
                    </IconCircle>
                    <PromotionDelivery>
                      Someday Delivery
                    </PromotionDelivery>
                  </Promotion>
                  </TouchableOpacity>
              </ProductContainer>
          </WidthFullSize>
          </Card>
          </ScrollView>  
          <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            alert('Modal has been closed.');
          }}>
          <SafeAreaView style={{ flex:1 }}>
          <View style={{marginTop: 20}}>
            <DistributeNyampingUjung>
              <View style={{marginLeft:10}}>
              <FontSizeXL><B>Filter</B></FontSizeXL>
              </View>
              <View style={{marginRight:10}}>
              <TouchableOpacity
                onPress={() => {
                  this.setModalVisible(!this.state.modalVisible);
                }}>
              <Ionicons
                    name="ios-close-circle"
                    size={28}
                    color="rgba(85, 87, 97, 1)"
                    style={{ alignSelf: "flex-end" }}
                  />
              </TouchableOpacity>
              </View>
            </DistributeNyampingUjung>
            <HR />
            <Container>
            <FontSizeM>Harga</FontSizeM>
               <FormM>
                <TextInput
                  placeholder="Minimum"
                  underlineColorAndroid="transparent"
                  selectionColor="rgba(242, 101, 37, 1)"
                  keyboardType="numeric"
                />
              </FormM>
              
              <FormM>
                <TextInput
                  placeholder="Maximum"
                  underlineColorAndroid="transparent"
                  selectionColor="rgba(242, 101, 37, 1)"
                  keyboardType="numeric"
                  
                />
              </FormM>
              <HR />
              <FontSizeM>Brand</FontSizeM>
              <FormMIcon>
                <Ionicons
                  name="md-search"
                  size={18}
                  color="rgba(85, 87, 97, 1)"
                  style={{ alignSelf: "center" }}
                />
                <LeftM>
                  <TextInput
                    placeholder="Cari merek"
                    underlineColorAndroid="transparent"
                    selectionColor="rgba(242, 101, 37, 1)"
                    keyboardType="default"
                    style={{ width: 300 }}
                  />
                </LeftM>
              </FormMIcon>
              
              <CheckBox
                  style={{ padding: 5}}
                  onClick={()=>{
                    this.setState({
                        isChecked:!this.state.isChecked
                    })
                  }}
                  isChecked={this.state.isChecked}
                  leftText={"CheckBox"}
                  leftTextStyle={{color: "#555761"}}
                  uncheckedCheckBoxColor={"#555761"}
                  checkedCheckBoxColor={"#008CCF"}
              />
              
              
            </Container>
          </View>
          </SafeAreaView>
        </Modal>
          
      </Bg>
    );
  }
}

export default ProductCategoryPage;
