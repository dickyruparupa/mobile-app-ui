import React from 'react';
import { StyleSheet, Text, View, SafeAreaView, ScrollView, Dimensions } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import { createDrawerNavigator, DrawerItems } from 'react-navigation';

import GuideLine from './pages/GuideLine';
import ProductCategoryPage from './pages/Pcp';
import DashboardPage from './pages/Dashboard';
import ProductDetailPage from './pages/Pdp';
import HomePage from './pages/HomePage';
import Kategori from './pages/Kategori';
export default class App extends React.Component{
  render(){
      return (
        <AppDrawerNavigator />
      );    
  }
}

const CustomDrawerComponent = (props) => (
  <SafeAreaView style={{ flex:1, marginTop:20 }}>
    <ScrollView>
        <DrawerItems {...props}/>
    </ScrollView>  
  </SafeAreaView>
)

const AppDrawerNavigator = createDrawerNavigator({
  GuideLine: GuideLine,
  PCP: ProductCategoryPage,
  Dashboard: DashboardPage,
  PDP: ProductDetailPage,
  Index: HomePage,
  Kategori: Kategori
}, {
  contentComponent: CustomDrawerComponent,
  contentOptions:{
    activeTintColor:'#F26525'
  }
})

const AppStackNavigator = createStackNavigator({
  GuideLine: GuideLine,
  Pcp: ProductCategoryPage,
  Dashboard: DashboardPage
})

