import React from "react";
import styled from "styled-components";
import {
  View,
  Text,
  ScrollView,
  Button,
  TouchableOpacity,
  TextInput,
  Picker,
  Animated,
  StyleSheet
} from "react-native";
import { Ionicons , EvilIcons , FontAwesome } from "@expo/vector-icons";
import Image from "react-native-remote-svg";



export default class Semua extends React.Component {
  render() {
    return (
      <Bg>
        <CardHeader>
          <DistributeNyampingUjung>
            <TouchableOpacity>
              <Ionicons
                name="md-menu"
                size={32}
                color="rgba(85, 87, 97, 1)"
                style={{}}
              />
            </TouchableOpacity>
            <Image
              source={{
                uri:
                  "https://res.cloudinary.com/ruparupa-com/image/upload/v1524564956/ruparupa-assets/img/ruparupa-logo.svg"
              }}
              style={{ height: 25, alignSelf: 'center' }}
            />
            <TouchableOpacity>
              <Ionicons
                name="md-cart"
                size={32}
                color="rgba(85, 87, 97, 1)"
                style={{}}
              />
            </TouchableOpacity>
          </DistributeNyampingUjung>
          <TouchableOpacity>
            <FormMainSearch>
              <DistributeNyampingUjung>
                <TextMainSearch>Cari ruparupa produk atau merek</TextMainSearch>
                <BtnMainSearch>
                  <Ionicons
                    name="md-search"
                    size={22}
                    color="rgba(255, 255, 255, 1)"
                    style={{ alignSelf: "center" }}
                  />
                </BtnMainSearch>
              </DistributeNyampingUjung>
            </FormMainSearch>
          </TouchableOpacity>
        </CardHeader>
        <ScrollView>
          <CardTitle>Header Selain Index</CardTitle>
          <CardHeader>
            <DistributeNyampingUjung>
              <TouchableOpacity>
                <Ionicons
                  name="md-menu"
                  size={32}
                  color="rgba(85, 87, 97, 1)"
                  style={{ marginLeft: 5 }}
                />
              </TouchableOpacity>
              <TouchableOpacity>
                <FormSecondarySearch>
                  <Ionicons
                    name="md-search"
                    size={18}
                    color="rgba(85, 87, 97, 1)"
                    style={{ alignSelf: "center" }}
                  />
                  <TextSecondarySearch>Cari di <TextGray>rup</TextGray><TextBlue>a</TextBlue><TextGray>rup</TextGray><TextOrange>a</TextOrange> </TextSecondarySearch>
                </FormSecondarySearch>
              </TouchableOpacity>
              <TouchableOpacity>
                <Ionicons
                  name="md-cart"
                  size={32}
                  color="rgba(85, 87, 97, 1)"
                  style={{ marginRight: 5 }}
                />
              </TouchableOpacity>
            </DistributeNyampingUjung>
          </CardHeader>
          <CardTitle>Header Selain Index PCP</CardTitle>
          <CardHeader>
            <DistributeNyampingUjung>
              <TouchableOpacity>
                <Ionicons
                  name="md-menu"
                  size={32}
                  color="rgba(85, 87, 97, 1)"
                  style={{ marginLeft: 5 }}
                />
              </TouchableOpacity>
              <TouchableOpacity>
                <FormSecondarySearch>
                  <Ionicons
                    name="md-search"
                    size={18}
                    color="rgba(85, 87, 97, 1)"
                    style={{ alignSelf: "center" }}
                  />
                  <TextSecondarySearch>Cari di <TextGray>rup</TextGray><TextBlue>a</TextBlue><TextGray>rup</TextGray><TextOrange>a</TextOrange> </TextSecondarySearch>
                </FormSecondarySearch>
              </TouchableOpacity>
              <TouchableOpacity>
                <Ionicons
                  name="md-cart"
                  size={32}
                  color="rgba(85, 87, 97, 1)"
                  style={{ marginRight: 5 }}
                />
              </TouchableOpacity>
            </DistributeNyampingUjung>
          </CardHeader>
          <CardSubHeader>
            <Row>
              <HeaderPills>
                <Ionicons
                  name="ios-funnel-outline"
                  size={14}
                  color="rgba(85, 87, 97, 1)"
                  style={{}}
                /><LeftM>
                  <Center><FontSizeXS>Filter</FontSizeXS></Center>
                </LeftM>
              </HeaderPills>
              <HeaderPills>
                <Ionicons
                  name="ios-list"
                  size={16}
                  color="rgba(85, 87, 97, 1)"
                  style={{alignSelf:'center'}}
                /><LeftM>
                  <FontSizeXS>Urut Berdasarkan</FontSizeXS>
                </LeftM>
              </HeaderPills>
            </Row>
          </CardSubHeader>
          
          <CardTitle>Info box</CardTitle>
          <InfoBoxPcp>
            <Ionicons
              name="ios-information-circle"
              size={16}
              color="rgba(85, 87, 97, 1)"
              style={{ alignSelf: "flex-start" }}
            />
            <LeftM>
              <FontSizeS>Bacod goes here</FontSizeS>
            </LeftM>
          </InfoBoxPcp>
          <Container>
            <CardTitle>Input</CardTitle>
            <Card>
              <LabelForm>Label Form</LabelForm>
              <FormM>
                <TextInput
                  placeholder="Placeholder text"
                  underlineColorAndroid="transparent"
                  selectionColor="rgba(242, 101, 37, 1)"
                  keyboardType="default"
                />
              </FormM>
              <LabelForm>Form with Icon</LabelForm>
              <FormMIcon>
                <Ionicons
                  name="md-search"
                  size={18}
                  color="rgba(85, 87, 97, 1)"
                  style={{ alignSelf: "center" }}
                />
                <LeftM>
                  <TextInput
                    placeholder="Placeholder text"
                    underlineColorAndroid="transparent"
                    selectionColor="rgba(242, 101, 37, 1)"
                    keyboardType="default"
                    style={{ width: 300 }}
                  />
                </LeftM>
              </FormMIcon>
              <LabelForm>Numeric Form</LabelForm>
              <FormM>
                <TextInput
                  placeholder="Placeholder text"
                  underlineColorAndroid="transparent"
                  selectionColor="rgba(242, 101, 37, 1)"
                  keyboardType="numeric"
                />
              </FormM>
            </Card>
            <CardTitle>Layout</CardTitle>
            <Left>
              <FontSizeL>Kiri</FontSizeL>
            </Left>
            <Center>
              <FontSizeL>Tengah</FontSizeL>
            </Center>
            <Right>
              <FontSizeL>Kanan</FontSizeL>
            </Right>
            <HR />
            <DistributeNyampingUjung>
              <FontSizeM>Satu</FontSizeM>
              <FontSizeM>Dua</FontSizeM>
              <FontSizeM>Tiga</FontSizeM>
            </DistributeNyampingUjung>
            <CardTitle>Buttons</CardTitle>
            <ButtonPrimaryM>
              <ButtonPrimaryMText>Primary Button</ButtonPrimaryMText>
            </ButtonPrimaryM>
            <ButtonPrimaryInverseM>
              <ButtonPrimaryInverseMText>
                Primary Inverse Button
              </ButtonPrimaryInverseMText>
            </ButtonPrimaryInverseM>
            <ButtonSecondaryM>
              <ButtonSecondaryMText>Secondary Button</ButtonSecondaryMText>
            </ButtonSecondaryM>
            <ButtonSecondaryInverseM>
              <ButtonSecondaryInverseMText>
                Secondary Inverse Button
              </ButtonSecondaryInverseMText>
            </ButtonSecondaryInverseM>
            <ButtonGrayInverseM>
              <ButtonGrayInverseMText>
                Gray Inverse Button
              </ButtonGrayInverseMText>
            </ButtonGrayInverseM>
            <ButtonPrimaryMDisabled disabled={true}>
              <ButtonPrimaryMText>Primary Button</ButtonPrimaryMText>
            </ButtonPrimaryMDisabled>
            <ButtonSecondaryMDisabled disabled={true}>
              <ButtonPrimaryMText>Secondary Button</ButtonPrimaryMText>
            </ButtonSecondaryMDisabled>
            <CardTitle>Heading</CardTitle>
            <Card>
              <H2>
                <B>(H2)</B> The quick brown fox jumps over the lazy dog
              </H2>
              <HR />
              <H3>
                <B>(H3)</B> The quick brown fox jumps over the lazy dog
              </H3>
              <HR />
              <H4>
                <B>(H4)</B> The quick brown fox jumps over the lazy dog
              </H4>
              <HR />
              <P>
                <B>(P)</B> The quick brown fox jumps over the lazy dog
              </P>
            </Card>
            <CardTitle>Font Sizes</CardTitle>
            <Card>
              <FontSizeXL>
                <B>(XL)</B> The quick brown fox jumps over the lazy dog
              </FontSizeXL>
              <HR />
              <FontSizeL>
                <B>(L)</B> The quick brown fox jumps over the lazy dog
              </FontSizeL>
              <HR />
              <FontSizeM>
                <B>(M)</B> The quick brown fox jumps over the lazy dog
              </FontSizeM>
              <HR />
              <FontSizeS>
                <B>(S)</B> The quick brown fox jumps over the lazy dog
              </FontSizeS>
              <HR />
              <FontSizeXS>
                <B>(XS)</B> The quick brown fox jumps over the lazy dog
              </FontSizeXS>
              <HR />
              <FontSizeXXS>
                <B>(XXS)</B> The quick brown fox jumps over the lazy dog
              </FontSizeXXS>
            </Card>           
          </Container>
        </ScrollView>
      </Bg>
    );
  }
}
const Bg = styled.View`
  background-color: #f9fafc;
  margin-top: 20px;
  flex: 1;
`;
const Container = styled.View`
  padding: 10px;
`;
const BgText = styled.Text`
  color: red !important;
`;
const Card = styled.View`
  background-color: white;
  padding: 15px;
  box-shadow: 1px 1px 1px #d4dce6;
  border-radius: 3px;
  elevation: 1;
`;
const CardHeader = styled.View`
background-color: white;
padding-top:20px
padding-left:10px;
padding-right:10px;
padding-bottom:5px;
box-shadow: 1px 1px 1px #D4DCE6;
elevation:2;
`;

const CardSubHeader = styled.View`
background-color: white;
padding-top:0px
padding-left:10px;
padding-right:10px;
padding-bottom:5px;
box-shadow: 1px 1px 1px #D4DCE6;
elevation:1;
`;
const FormMainSearch = styled.View`
  border: 1px #e5e9f2 solid;
  padding-left: 10px;
  padding-right: 0px;
  border-radius: 3px;
  margin-top: 5px;
  margin-bottom: 10px;
  align-self: stretch;
`;
const TextMainSearch = styled.Text`
  color: #f26525;
  font-weight: bold;
  font-size: 14px;
`;
const FormSecondarySearch = styled.View`
  border: 1px #e5e9f2 solid;
  padding-left: 10px;
  padding-right: 0px;
  border-radius: 3px;
  margin-top: 5px;
  margin-bottom: 10px;
  flex-direction: row;
`;
const TextSecondarySearch = styled.Text`
  color: #555761;
  font-size: 14px;
  padding:10px;
`;
const TextOrange = styled.Text`
color: #F26525;
font-weight: bold;
`;
const TextBlue = styled.Text`
color: #008CCF;
font-weight: bold;
`;
const TextGray = styled.Text`
color: #555761;
font-weight: bold;
`;
const BtnMainSearch = styled.View`
  background-color: #f26525;
  padding-top: 10px;
  padding-bottom: 10px;
  padding-left: 20px;
  padding-right: 20px;
  border-radius: 3px;
  box-shadow: 1px 1px 1px #f26525;
`;
const CardTitle = styled.Text`
  font-size: 20px;
  font-weight: bold;
  padding-left: 5px;
  padding-top: 5px;
  margin-top: 15px;
  color: #555761;
  line-height: 24px;
  margin-bottom: 10px;
`;
const H1 = styled.Text`
  font-size: 36px !important;
  color: #555761;
  line-height: 42px;
`;
const H2 = styled.Text`
  font-size: 28px !important;
  color: #555761;
  line-height: 36px;
`;
const H3 = styled.Text`
  font-size: 20px !important;
  color: #555761;
  line-height: 28px;
`;
const H4 = styled.Text`
  font-size: 18px !important;
  color: #555761;
  line-height: 20px;
`;
const P = styled.Text`
  font-size: 14px;
  color: #555761;
  line-height: 18px;
`;
const B = styled.Text`
  font-weight: bold;
`;
const HR = styled.View`
  border: 0.5px solid #e5e9f2;
  margin-top: 10px;
  margin-bottom: 10px;
`;
const FontSizeXXXL = styled.Text`
  font-size: 36px !important;
  color: #555761;
  line-height: 42px;
`;
const FontSizeXXL = styled.Text`
  font-size: 28px !important;
  color: #555761;
  line-height: 36px;
`;
const FontSizeXL = styled.Text`
  font-size: 20px !important;
  color: #555761;
  line-height: 28px;
`;
const FontSizeL = styled.Text`
  font-size: 18px !important;
  color: #555761;
  line-height: 20px;
`;
const FontSizeM = styled.Text`
  font-size: 16px !important;
  color: #555761;
  line-height: 18px;
`;
const FontSizeS = styled.Text`
  font-size: 14px !important;
  color: #555761;
  line-height: 16px;
`;
const FontSizeXS = styled.Text`
  font-size: 12px !important;
  color: #555761;
  line-height: 14px;
`;
const FontSizeXXS = styled.Text`
  font-size: 10px !important;
  color: #555761;
  line-height: 12px;
`;

// Margin
const MarginXL = styled.View`
  margin: 60px;
`
const MarginL = styled.View`
  margin: 25px;
`
const MarginM = styled.View`
  margin: 20px;
`
const MarginS = styled.View`
  margin: 15px;
`
const MarginXS = styled.View`
  margin: 10px;
`
const MarginXXS = styled.View`
  margin: 5px;
`
// Margin Top

const MarginTopXL = styled.View`
  margin-top: 60px;
`;
const MarginTopL = styled.View`
  margin-top: 25px;
`; 
const MarginTopS = styled.View`
  margin-top: 15px;
`;
const MarginTopXS = styled.View`
  margin-top: 10px;
`;
const MarginTopXXS = styled.View`
  margin-top: 5px;
`;
// Margin Bottom

const MarginBottomXL = styled.View`
  margin-bottom: 60px;
`;
const MarginBottomL = styled.View`
  margin-bottom: 25px;
`; 
const MarginBottomS = styled.View`
  margin-bottom: 15px;
`;
const MarginBottomXS = styled.View`
  margin-bottom: 10px;
`;
const MarginBottomXXS = styled.View`
  margin-bottom: 5px;
`;
// Margin Left

const MarginLeftXL = styled.View`
margin-Left: 60px;
`;
const MarginLeftL = styled.View`
margin-Left: 25px;
`; 
const MarginLeftS = styled.View`
margin-Left: 15px;
`;
const MarginLeftXS = styled.View`
margin-Left: 10px;
`;
const MarginLeftXXS = styled.View`
margin-Left: 5px;
`;
// margin Right

const MarginRightXL = styled.View`
margin-Left: 60px;
`;
const MarginRightL = styled.View`
margin-Left: 25px;
`; 
const MarginRightS = styled.View`
margin-Left: 15px;
`;
const MarginRightXS = styled.View`
margin-Left: 10px;
`;
const MarginRightXXS = styled.View`
margin-Left: 5px;
`;


// padding
const PaddingXL = styled.View`
padding: 60px;
`
const PaddingL = styled.View`
padding: 25px;
`
const PaddingM = styled.View`
padding: 20px;
`
const PaddingS = styled.View`
padding: 15px;
`
const PaddingXS = styled.View`
padding: 10px;
`
const PaddingXXS = styled.View`
padding: 5px;
`
// padding Top

const PaddingTopXL = styled.View`
padding-top: 60px;
`;
const PaddingTopL = styled.View`
padding-top: 25px;
`; 
const PaddingTopS = styled.View`
padding-top: 15px;
`;
const PaddingTopXS = styled.View`
padding-top: 10px;
`;
const PaddingTopXXS = styled.View`
padding-top: 5px;
`;
// padding Bottom

const PaddingBottomXL = styled.View`
padding-bottom: 60px;
`;
const PaddingBottomL = styled.View`
padding-bottom: 25px;
`; 
const PaddingBottomS = styled.View`
padding-bottom: 15px;
`;
const PaddingBottomXS = styled.View`
padding-bottom: 10px;
`;
const PaddingBottomXXS = styled.View`
padding-bottom: 5px;
`;
// padding Left

const PaddingLeftXL = styled.View`
padding-Left: 60px;
`;
const PaddingLeftL = styled.View`
padding-Left: 25px;
`; 
const PaddingLeftS = styled.View`
padding-Left: 15px;
`;
const PaddingLeftXS = styled.View`
padding-Left: 10px;
`;
const PaddingLeftXXS = styled.View`
padding-Left: 5px;
`;
// padding Right

const PaddingRightXL = styled.View`
padding-Left: 60px;
`;
const PaddingRightL = styled.View`
padding-Left: 25px;
`; 
const PaddingRightS = styled.View`
padding-Left: 15px;
`;
const PaddingRightXS = styled.View`
padding-Left: 10px;
`;
const PaddingRightXXS = styled.View`
padding-Left: 5px;
`;
const ButtonPrimaryM = styled.TouchableOpacity`
  background-color: #f26525;
  padding: 10px;
  border-radius: 3px;
  margin: 5px;
`;
const ButtonPrimaryMDisabled = styled.TouchableOpacity`
  background-color: #f26525;
  padding: 10px;
  border-radius: 3px;
  margin: 5px;
  opacity: 0.5;
`;
const ButtonPrimaryMText = styled.Text`
  font-size: 16px;
  color: white;
  text-align: center;
  font-weight: bold;
`;
const ButtonPrimaryInverseM = styled.TouchableOpacity`
  background-color: white;
  border: 1px #f26525 solid;
  padding: 10px;
  border-radius: 3px;
  margin: 5px;
`;
const ButtonPrimaryInverseMText = styled.Text`
  font-size: 16px;
  color: #f26525;
  text-align: center;
`;
const ButtonGrayInverseM = styled.TouchableOpacity`
  background-color: white;
  border: 1px #757885 solid;
  padding: 10px;
  border-radius: 3px;
  margin: 5px;
`;
const ButtonGrayInverseMText = styled.Text`
  font-size: 16px;
  color: #757885;
  text-align: center;
`;
const ButtonSecondaryM = styled.TouchableOpacity`
  background-color: #008ccf;
  padding: 10px;
  border-radius: 3px;
  margin: 5px;
`;
const ButtonSecondaryMDisabled = styled.TouchableOpacity`
  background-color: #008ccf;
  padding: 10px;
  border-radius: 3px;
  margin: 5px;
  opacity: 0.5;
`;
const ButtonSecondaryMText = styled.Text`
  font-size: 16px;
  color: white;
  text-align: center;
  font-weight: bold;
`;
const ButtonSecondaryInverseM = styled.TouchableOpacity`
  background-color: white;
  border: 1px #008ccf solid;
  padding: 10px;
  border-radius: 3px;
  margin: 5px;
`;
const ButtonSecondaryInverseMText = styled.Text`
  font-size: 16px;
  color: #008ccf;
  text-align: center;
`;
const Left = styled.View`
  align-items: flex-start;
  margin-left: 5px;
`;
const Center = styled.View`
  align-items: center;
`;
const Right = styled.View`
  align-items: flex-end;
  margin-right: 5px;
`;
const DistributeNyampingUjung = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;
const DistributeNyampingTengah = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-around;
`;
const HeaderPills = styled.TouchableOpacity`
flex:0.5;
flex-direction:row;
justify-content:center;
align-items:center;
border: 1px solid #E5E9F2;
padding:5px;
border-radius:5px;
margin-top:10px;
margin-left:5px;
margin-right:5px;
margin-bottom:5px;
`;
const FormM = styled.View`
  border: 1px #e5e9f2 solid;
  padding: 10px;
  border-radius: 3px;
  margin-top: 5px;
  margin-bottom: 10px;
`;
const FormMIcon = styled.View`
  flex-direction: row;
  border: 1px #e5e9f2 solid;
  align-self: stretch;
  padding: 10px;
  border-radius: 3px;
  margin-top: 5px;
  margin-bottom: 10px;
`;
const LabelForm = styled.Text`
  font-size: 14px;
  color: #555761;
  font-weight: bold;
`;
const LeftM = styled.View`
  margin-left: 5px;
`;
const InfoBoxPcp = styled.View`
  flex-direction: row;
  flex: 1;
  background-color: #e5f7ff;
  padding: 10px;
`;
const NotifBadge = styled.View`
  border-radius:100px;
  background-color:#E5E9F2;
  padding:7px;
  margin-5px;
  width:26px;
`;
const NotifText = styled.Text`
  color:#555761;
  font-size:10px;
  font-weight:bold;
  text-align:center;
`;

const Row = styled.View`
flex-direction:row;
flex:1;
`;


// Card
const ProductContainer = styled.TouchableOpacity`
  background-color: #ffffff;
  border-radius: 3px;
  margin-bottom: 20px;
  position: relative;
  padding-bottom: 1px;
  padding: 10px;
  margin-left: 5px;
  margin-right: 5px;
  margin-top: 10px;
  flex: 0.5;
`;
const WidthFullSize = styled.View`
  flex: 1;
  flexDirection: row;
`;
const Priceold = styled.Text`
  color: #757886;
  position: relative;
`;
const Priceoldhr = styled.View`
  width: 70%;
  height: 1px;
  background-color: #f26524;
  top: 6px;
  position: absolute;
`;

const PriceProduct = styled.View`
  width: 70%;
  margin-bottom: 5px;
`;

const Price = styled.Text`
  font-size: 16px;
  line-height: 19px;
  color: #008ed1;
  font-weight: 700;
`;

const TitleLimitName = styled.Text`
  height: 38px;
  color: rgba(0, 0, 0, 0.7);
  overflow: hidden;
  line-height: 18px;
  opacity: 0.7;
`;
const DiscountContainer = styled.View`
  width: 30px;
  height: 30px;
  background: #f3591f;
  border-radius: 25px;
  position: absolute;
  left: 130px;
  padding-top: 8px;
  padding-left: 3px;
`;
const TextDiscount = styled.Text`
  font-size: 10px;
  color: white;
`;
const Wishlist = styled.View`
  margin-left: 5px;
`

const Promotion = styled.View`
  flex: 1;
  flexDirection: row;
  margin-top: 10px;
  margin-bottom: 10px;
  opacity: 0.7;
`;

const IconCircle = styled.View`
  flex: 0.15;
  margin-top: 2px;
`;

const PromotionText = styled.Text`
  flex: 0.95;
  color: red;
  font-weight: 700;
  font-size: 12px;
`;