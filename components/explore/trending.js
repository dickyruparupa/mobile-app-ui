import React, { Component } from 'react';
import { Text, View, Image, StyleSheet } from 'react-native';

class ExploreByTrending extends Component {
  render() {
    return (
        <View style={{height:170, width:150,  marginLeft:11,  borderRadius:3, backgroundColor:'#FFFFFF', shadowColor: '#757885',
        shadowOffset: { width: 1, height: 1 },shadowOpacity: 0.2,shadowRadius: 4, elevation: 1}}>
            <View style={{flex:3}}>
            <Image source={this.props.imageUri}
            style={{flex:1, width:null, height:null, resizeMode:'cover'}}
            />
            </View>
            <View style={{flex:1, padding:5}}>
            <Text style={{textAlign:'center', alignSelf:'center'}}>{this.props.name}</Text>
            </View>  
    </View>  
    );
  }
}

export default ExploreByTrending;
